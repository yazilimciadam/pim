-- Adminer 4.8.1 MySQL 8.0.30 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

USE `pim_agency`;

SET NAMES utf8mb4;

INSERT INTO `about` (`about_id`, `about`, `facebook`, `linkedin`, `instagram`, `mail`, `phone`, `address`) VALUES
(1,	'<p>PIM was founded by Architect Sevda &Ccedil;alışkan and Architect Emre Yiğit in 2017. PIM is a company that aims to design &lsquo;respectful&rsquo; and original places by keeping customer satisfaction in the forefront in a sustainable, ecofriendly and human sensitive mindset.</p>\r\n<p>&nbsp;</p>\r\n<p>As PIM, we take care to design innovative buildings that incudes breathing spaces for people and blend functionality with technology in both architectural and interior design projects. We apply innovative and dynamic corporate identity designs specific to your brand in your places with our young staff. We direct our work with the awareness of being a solution partner with our customers in the context of new generation office and new generation team.</p>\r\n<p>&nbsp;</p>\r\n<p>The life-time of a building is hidden in the details that are correctly solved. PIM targets the right solution on details for different type of buildings such as hotels, residences, offices, hospitals and factories with the experienced team.</p>',	'https://www.facebook.com/PimArchitects/',	'https://www.linkedin.com/company/pimarchitects/',	'https://instagram.com/pimarchitects',	'',	'',	'[\"<p>adres1</p>\",\"<p>adres2</p>\"]')
ON DUPLICATE KEY UPDATE `about_id` = VALUES(`about_id`), `about` = VALUES(`about`), `facebook` = VALUES(`facebook`), `linkedin` = VALUES(`linkedin`), `instagram` = VALUES(`instagram`), `mail` = VALUES(`mail`), `phone` = VALUES(`phone`), `address` = VALUES(`address`);

INSERT INTO `about_personal` (`per_id`, `name`, `text`, `image`) VALUES
(1,	'Sevda Çalışkan',	'<p>Sevda graduated from Istanbul Technical University, Department of Architecture in 2011. After that she started Istanbul Technical University Project and Construction Management Master Program. Sevda completed her thesis entitled &rsquo;The Role of Communication in Construction Projects: A Comparative Analysis of Successful and Unsuccessful Cases project&rsquo;.&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Also she has two published papers on project and production management. In her professional life, he took part in the design and implementation processes of many domestic and international projects such as residance, factory, warehouse and hotel. She has been working in PIM since 2017.</p>',	'images/1670789698.jpg'),
(2,	'Emre Yiğit',	'<p>Emre successfully completed the Architecture Department of Mimar Sinan Fine Arts University in 2009 and subsequently joined the ITU in Department of Informatics Architectural Design.&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>In his professional life, he took part in the design and implementation processes of many domestic and international projects such as residence, office, hospital, university, hotel and shopping center. He has been working in PIM since 2017</p>',	'images/1670789744.jpeg')
ON DUPLICATE KEY UPDATE `per_id` = VALUES(`per_id`), `name` = VALUES(`name`), `text` = VALUES(`text`), `image` = VALUES(`image`);

INSERT INTO `projects` (`project_id`, `project_title`, `project_location`, `project_area`, `project_images`, `project_banner`, `isHomePage`, `isHomePageSort`, `thumbnail`) VALUES
(1,	'KING HOME APPLIANCES OFFICE',	'ISTANBUL / TURKEY',	'850 m2',	'[{\"image\":\"images/1670784891.jpeg\"},{\"image\":\"images/1670784891_1.jpeg\"},{\"image\":\"images/1670784891_2.jpeg\"},{\"image\":\"images/1670784891_3.jpeg\"}]',	'images/1670784891_4.jpeg',	1,	0,	'images/aba389832218683.jpg'),
(7,	'B HOUSE',	'SOFIA / BULGARIA',	'400 m2',	'[{\"image\":\"images/1671181052.jpg\"},{\"image\":\"images/1671181052_1.jpg\"},{\"image\":\"images/1671181052_2.jpg\"}]',	'images/1671181052_3.jpg',	1,	1,	'images/aba389832229168.jpg'),
(8,	'GÜZELYALI YANSIMA',	'CANAKKALE / TURKEY',	'300 m2',	'[{\"image\":\"images/1671181266.jpg\"},{\"image\":\"images/1671181266_1.jpg\"},{\"image\":\"images/1671181266_2.jpg\"},{\"image\":\"images/1671181266_3.jpg\"},{\"image\":\"images/1671181266_4.jpg\"}]',	'images/1671181266_5.jpg',	1,	2,	'images/aba389832239886.jpg'),
(9,	'KOZLU HOUSE',	'BALIKESIR / TURKEY',	'400 m2',	'[{\"image\":\"images/1671181829.jpg\"},{\"image\":\"images/1671181829_1.jpg\"},{\"image\":\"images/1671181829_2.jpg\"},{\"image\":\"images/1671181829_3.jpg\"},{\"image\":\"images/1671181829_4.jpg\"},{\"image\":\"images/1671181829_5.jpg\"},{\"image\":\"images/1671181829_6.jpg\"},{\"image\":\"images/1671181829_7.jpg\"},{\"image\":\"images/1671181829_8.jpg\"}]',	'images/1671181829_9.jpg',	1,	3,	'images/aba389832250604.jpg'),
(10,	'IRAQ OIL MINISTERIAL OFFICE',	'BASRA / IRAQ',	'15.000 m2',	'[{\"image\":\"images/1671181942.jpg\"},{\"image\":\"images/1671181942_1.jpg\"}]',	'images/1671181942_2.jpg',	1,	4,	'images/aba389832271108.jpg'),
(11,	'ATAPARK APARTMENT',	'USAK / TURKEY',	'1.600 m2',	'[{\"image\":\"images/1671182000.jpg\"},{\"image\":\"images/1671182000_1.jpg\"}]',	'images/1671182000_2.jpg',	0,	4,	'images/aba389832285088.jpg'),
(12,	'AL KHARAEJ OFFICE',	'DOHA / QATAR',	'13.200 m2',	'[{\"image\":\"images/1671787675.jpg\"},{\"image\":\"images/1671787675_1.jpg\"}]',	'images/1671787675_2.jpg',	NULL,	NULL,	'images/aba389832291379.jpg'),
(13,	'UTSO  OFFICE',	'USAK / TURKEY',	'5.000 m2',	'[{\"image\":\"images/1671787835.jpg\"},{\"image\":\"images/1671787835_1.jpg\"},{\"image\":\"images/1671787835_2.jpg\"},{\"image\":\"images/1671787835_3.jpg\"},{\"image\":\"images/1671787835_4.jpg\"}]',	'images/1671787835_5.jpg',	NULL,	NULL,	'images/aba389832296272.jpg'),
(14,	'ROYAL BAKU HOUSE',	'BAKU / AZERBAIJAN',	'100.000 m2',	'[{\"image\":\"images/1671788072.jpg\"},{\"image\":\"images/1671788072_1.jpg\"},{\"image\":\"images/1671788072_2.jpg\"},{\"image\":\"images/1671788072_3.jpg\"},{\"image\":\"images/1671788072_4.jpg\"},{\"image\":\"images/1671788072_5.jpg\"}]',	'images/1671788072_6.jpg',	NULL,	NULL,	'images/aba389832300233.jpg'),
(15,	'UZBEKISTAN SPORT CENTER',	'TASHKENT / UZBEKISTAN',	'2.000 m2',	'[{\"image\":\"images/1671788136.jpg\"},{\"image\":\"images/1671788136_1.jpg\"},{\"image\":\"images/1671788136_2.jpg\"},{\"image\":\"images/1671788136_3.jpg\"},{\"image\":\"images/1671788136_4.jpg\"},{\"image\":\"images/1671788136_5.jpg\"},{\"image\":\"images/1671788136_6.jpg\"}]',	'images/1671788136_7.jpg',	NULL,	NULL,	'images/aba389832303728.jpg'),
(16,	'AKDERMAK OFFICE',	'USAK / TURKEY',	'1.000 m2',	'[{\"image\":\"images/1671788193.jpg\"},{\"image\":\"images/1671788193_1.jpg\"}]',	'images/1671788193_2.jpg',	NULL,	NULL,	'images/aba389832308621.jpg'),
(17,	'WINTER HOUSE',	'BALIKESIR / TURKEY',	'150 m2',	'[{\"image\":\"images/1671788248.jpg\"},{\"image\":\"images/1671788248_1.jpg\"},{\"image\":\"images/1671788248_2.jpg\"},{\"image\":\"images/1671788248_3.jpg\"},{\"image\":\"images/1671788248_4.jpg\"},{\"image\":\"images/1671788248_5.jpg\"},{\"image\":\"images/1671788248_6.jpg\"},{\"image\":\"images/1671788248_7.jpg\"}]',	'images/1671788248_8.jpg',	NULL,	NULL,	'images/aba389832323766.jpg'),
(18,	'TASHKENT UNIVERSITY',	'TASHKENT / UZBEKISTAN',	'2.000 m2',	'[{\"image\":\"images/1671788321.jpg\"},{\"image\":\"images/1671788321_1.jpg\"},{\"image\":\"images/1671788321_2.jpg\"},{\"image\":\"images/1671788321_3.jpg\"},{\"image\":\"images/1671788321_4.jpg\"},{\"image\":\"images/1671788321_5.jpg\"},{\"image\":\"images/1671788321_6.jpg\"},{\"image\":\"images/1671788321_7.jpg\"},{\"image\":\"images/1671788321_8.jpg\"},{\"image\":\"images/1671788321_9.jpg\"},{\"image\":\"images/1671788321_10.jpg\"},{\"image\":\"images/1671788321_11.jpg\"}]',	'images/1671788321_12.jpg',	NULL,	NULL,	'images/aba389832328892.jpg'),
(19,	'TURK TELEKOM OFFICE',	'ISTANBUL / TURKEY',	'120 m2',	'[{\"image\":\"images/1671788388.jpg\"},{\"image\":\"images/1671788388_1.jpg\"},{\"image\":\"images/1671788388_2.jpg\"},{\"image\":\"images/1671788388_3.jpg\"},{\"image\":\"images/1671788388_4.jpg\"},{\"image\":\"images/1671788388_5.jpg\"},{\"image\":\"images/1671788388_6.jpg\"},{\"image\":\"images/1671788388_7.jpg\"}]',	'images/1671788388_8.jpg',	NULL,	NULL,	'images/aba389832332620.jpg'),
(20,	'2M DENTAL CLINIC',	'ISTANBUL / TURKEY',	'200 m2',	'[{\"image\":\"images/1671788442.jpg\"},{\"image\":\"images/1671788442_1.jpg\"},{\"image\":\"images/1671788442_2.jpg\"},{\"image\":\"images/1671788442_3.jpg\"}]',	'images/1671788442_4.jpg',	NULL,	NULL,	'images/aba389832337280.jpg'),
(21,	'AYVALIK HE HOUSE',	'BALIKESIR / TURKEY',	'220 m2',	'[{\"image\":\"images/1671788475.jpg\"},{\"image\":\"images/1671788475_1.jpg\"},{\"image\":\"images/1671788475_2.jpg\"}]',	'images/1671788475_3.jpg',	NULL,	NULL,	'images/aba389832341474.jpg'),
(22,	'SISLI HOUSE',	'ISTANBUL / TURKEY',	'100 m2',	'[{\"image\":\"images/1671788546.jpg\"},{\"image\":\"images/1671788546_1.jpg\"},{\"image\":\"images/1671788546_2.jpg\"}]',	'images/1671788546_3.jpg',	NULL,	NULL,	'images/aba389832348697.jpg'),
(23,	'CEKMEKOY HOUSE',	'ISTANBUL / TURKEY',	'400 m2',	'[{\"image\":\"images/1671788584.jpg\"},{\"image\":\"images/1671788584_1.jpg\"},{\"image\":\"images/1671788584_2.jpg\"}]',	'images/1671788584_3.jpg',	NULL,	NULL,	'images/aba389832352658.jpg'),
(24,	'OZCAN PICKLE SHOP',	'ISTANBUL / TURKEY',	'40 m2',	'[{\"image\":\"images/1671788657.jpg\"},{\"image\":\"images/1671788657_1.jpg\"},{\"image\":\"images/1671788657_2.jpg\"}]',	'images/1671788657_3.jpg',	NULL,	NULL,	'images/aba389832359182.jpg'),
(25,	'GUZELYALI YANSIMA',	'CANAKKALE / TURKEY',	'3.000 m2',	'[{\"image\":\"images/1671788705.jpg\"},{\"image\":\"images/1671788705_1.jpg\"},{\"image\":\"images/1671788705_2.jpg\"},{\"image\":\"images/1671788705_3.jpg\"}]',	'images/1671788705_4.jpg',	NULL,	NULL,	'images/aba389832367570.jpg'),
(26,	'PIM TASHKENT OFFICE',	'TASHKENT / UZBEKISTAN',	'200 m2',	'[{\"image\":\"images/1671788736.jpg\"},{\"image\":\"images/1671788736_1.jpg\"},{\"image\":\"images/1671788736_2.jpg\"}]',	'images/1671788736_3.jpg',	NULL,	NULL,	'images/aba389832373628.jpg'),
(27,	'LAL APARTMENT',	'USAK / TURKEY',	'1.000 m2',	'[{\"image\":\"images/1671788815.jpg\"}]',	'images/1671788815_1.jpg',	NULL,	NULL,	'images/aba389832378521.jpg'),
(28,	'BARER BOUTIQUE HOTEL',	'TASHKENT / UZBEKISTAN',	'975 m2',	'[{\"image\":\"images/1671789068.jpg\"},{\"image\":\"images/1671789068_1.jpg\"},{\"image\":\"images/1671789068_2.jpg\"}]',	'images/1671789068_3.jpg',	NULL,	NULL,	'images/aba389832382715.jpg'),
(29,	'POCKET  BOX',	'ISTANBUL / TURKEY',	'30 m2',	'[{\"image\":\"images/1673171164.jpg\"}]',	'images/1672659808_1.jpg',	NULL,	NULL,	'images/aba389832388540.jpg'),
(30,	'USAK BAZAAR',	'USAK / TURKEY',	'1.000 m2',	'[{\"image\":\"images/1671789209.jpg\"}]',	'images/1671789209_1.jpg',	NULL,	NULL,	'images/aba389832397161.jpg'),
(31,	'KOZYATAGI APARTMENT',	'ISTANBUL / TURKEY',	'1.600 m2',	'[{\"image\":\"images/1671789327.jpg\"}]',	'images/1671789327_1.jpg',	NULL,	NULL,	'images/aba389832405549.jpg'),
(32,	'SANAYI CAFE',	'ISTANBUL / TURKEY',	'100 m2',	'[{\"image\":\"images/1671789367.jpg\"},{\"image\":\"images/1671789367_1.jpg\"},{\"image\":\"images/1671789367_2.jpg\"}]',	'images/1671789367_3.jpg',	NULL,	NULL,	'images/aba389832419529.jpg'),
(33,	'UOSB FITNESS',	'USAK / TURKEY',	'2.000 m2',	'[{\"image\":\"images/1671789402.jpg\"}]',	'images/1671789402_1.jpg',	NULL,	NULL,	'images/aba389832426985.jpg'),
(34,	'OZER HOUSE',	'ISTANBUL / TURKEY',	'200 m2',	'[{\"image\":\"images/1671789436.jpg\"},{\"image\":\"images/1671789436_1.jpg\"},{\"image\":\"images/1671789436_2.jpg\"}]',	'images/1671789436_3.jpg',	NULL,	NULL,	'images/aba389832439334.jpg'),
(35,	'MANZARA RESTAURANT',	'CANAKKALE / TURKEY',	'1.000 m2',	'[{\"image\":\"images/1671789475.jpg\"},{\"image\":\"images/1671789475_1.jpg\"},{\"image\":\"images/1671789475_2.jpg\"}]',	'images/1671789475_3.jpg',	NULL,	NULL,	'images/aba389832434208.jpg'),
(36,	'R  OFFICE',	'SIVAS / TURKEY',	'12.400 m2',	'[{\"image\":\"images/1671790263.jpg\"}]',	'images/1671790263_1.jpg',	NULL,	NULL,	'images/aba389832450518.jpg'),
(37,	'MAHAL TIREBOLU',	'TIREBOLU / TURKEY',	'150.000 m2',	'[{\"image\":\"images/1671789828.jpg\"},{\"image\":\"images/1671789828_1.jpg\"},{\"image\":\"images/1671789828_2.jpg\"},{\"image\":\"images/1671789828_3.jpg\"}]',	'images/1671789828_4.jpg',	NULL,	NULL,	'images/aba389832456576.jpg'),
(38,	'CHOCOLATE CAFE',	'ISTANBUL / TURKEY',	'100 m2',	'[{\"image\":\"images/1671789887.jpg\"},{\"image\":\"images/1671789887_1.jpg\"}]',	'',	NULL,	NULL,	'images/aba389832502011.jpg'),
(39,	'MARKET  HALL',	'TOKAT / TURKEY',	'2.100 m2',	'[{\"image\":\"images/1671789955.jpg\"},{\"image\":\"images/1671789955_1.jpg\"}]',	'images/1671789955_2.jpg',	NULL,	NULL,	'images/aba389832468692.jpg'),
(40,	'PETERSBURG HOUSE',	'PETERSBURG / RUSSIA',	'2.400 m2',	'[{\"image\":\"images/1671789988.jpg\"}]',	'images/1671789988_1.jpg',	NULL,	NULL,	'images/aba389832511098.jpg'),
(41,	'LEVENT CLINIC',	'ISTANBUL / TURKEY',	'500 m2',	'[{\"image\":\"images/1671790021.jpg\"},{\"image\":\"images/1671790021_1.jpg\"},{\"image\":\"images/1671790021_2.jpg\"}]',	'images/1671790021_3.jpg',	NULL,	NULL,	'images/aba389832515292.jpg'),
(42,	'OZ WEEKEND HOUSE',	'USAK / TURKEY',	'150 m2',	'[{\"image\":\"images/1671790323.jpg\"}]',	'images/1671790323_1.jpg',	NULL,	NULL,	'images/aba389832519719.jpg'),
(43,	'OREN  HOUSE',	'MUGLA / TURKEY',	'5.000 m2',	'[{\"image\":\"images/1671790364.jpg\"}]',	'images/1671790364_1.jpg',	NULL,	NULL,	'images/aba389832524146.jpg'),
(44,	'USAK AIRPORT OFFICE',	'USAK / TURKEY',	'100 m2',	'[{\"image\":\"images/1671790399.jpg\"},{\"image\":\"images/1671790399_1.jpg\"}]',	'images/1671790399_2.jpg',	NULL,	NULL,	'images/aba389832529039.jpg'),
(45,	'RESADIYE LIBRARY',	'TOKAT / TURKEY',	'1.800 m2',	'[{\"image\":\"images/1671790443.jpg\"},{\"image\":\"images/1671790443_1.jpg\"}]',	'images/1671790443_2.jpg',	NULL,	NULL,	'images/aba389832533000.jpg'),
(46,	'KARAKOY CAFE',	'ISTANBUL / TURKEY',	'100 m2',	'[{\"image\":\"images/1671790482.jpg\"}]',	'images/1671790482_1.jpg',	NULL,	NULL,	'images/aba389832539291.jpg'),
(47,	'TERRACE HOUSE',	'GIRESUN / TURKEY',	'150.000 m2',	'[{\"image\":\"images/1671790565.jpg\"},{\"image\":\"images/1671790565_1.jpg\"},{\"image\":\"images/1671790565_2.jpg\"}]',	'images/1671790565_3.jpg',	NULL,	NULL,	'images/aba389832543252.jpg'),
(48,	'KING HOME APPLIANCES OFFICE 2',	'ISTANBUL / TURKEY',	'850 m2',	'[{\"image\":\"images/1671874305.jpg\"},{\"image\":\"images/1671874305_1.jpg\"},{\"image\":\"images/1671874305_2.jpg\"},{\"image\":\"images/1671874305_3.jpg\"},{\"image\":\"images/1671874305_4.jpg\"},{\"image\":\"images/1671874305_5.jpg\"},{\"image\":\"images/1671874305_6.jpg\"}]',	'images/1671874305_7.jpg',	NULL,	NULL,	'images/aba389832549077.jpg'),
(49,	'YOZGAT  MIX  USED',	'YOZGAT / TURKEY',	'45.000 m2',	'[{\"image\":\"images/1671874954.jpg\"},{\"image\":\"images/1671874954_1.jpg\"}]',	'images/1671874954_2.jpg',	NULL,	NULL,	'images/aba389832553737.jpg')
ON DUPLICATE KEY UPDATE `project_id` = VALUES(`project_id`), `project_title` = VALUES(`project_title`), `project_location` = VALUES(`project_location`), `project_area` = VALUES(`project_area`), `project_images` = VALUES(`project_images`), `project_banner` = VALUES(`project_banner`), `isHomePage` = VALUES(`isHomePage`), `isHomePageSort` = VALUES(`isHomePageSort`), `thumbnail` = VALUES(`thumbnail`);

INSERT INTO `references_pim` (`ref_id`, `image`) VALUES
(4,	'images/2dental-350-1.png'),
(39,	'images/1670791019.png'),
(40,	'images/1670791041.png'),
(41,	'images/1670791050.png'),
(42,	'images/1670791060.png'),
(43,	'images/1670791074.png'),
(44,	'images/1670791082.png'),
(45,	'images/1670791091.png'),
(46,	'images/1670791100.png'),
(47,	'images/1670791110.png'),
(48,	'images/1670791123.png'),
(49,	'images/1670791132.png'),
(50,	'images/1670791182.png'),
(51,	'images/1670791189.png'),
(52,	'images/1670791210.png'),
(53,	'images/1670791222.png'),
(54,	'images/1670791232.png'),
(55,	'images/1670791243.png')
ON DUPLICATE KEY UPDATE `ref_id` = VALUES(`ref_id`), `image` = VALUES(`image`);

INSERT INTO `users` (`user_id`, `name`, `mail`, `password`) VALUES
(1,	'Selçuk',	'user@user.com',	'e10adc3949ba59abbe56e057f20f883e')
ON DUPLICATE KEY UPDATE `user_id` = VALUES(`user_id`), `name` = VALUES(`name`), `mail` = VALUES(`mail`), `password` = VALUES(`password`);

-- 2023-01-10 09:10:53