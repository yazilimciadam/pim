<?php ini_set('default_charset','UTF-8');header('Content-Type: text/html; charset=UTF-8');header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');header('Cache-Control: post-check=0, pre-check=0', false);header('Pragma: no-cache'); ?><!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">
<head>
<meta charset="UTF-8">
<title>Thank You | PIM ARCHITECTS</title>
<meta name="referrer" content="same-origin">
<link rel="canonical" href="https://pimarchitects.com/thank-you-for-contacting-us.php">
<meta name="google-site-verification" content="VCnOnu8apZKI4ETt2bXQiNohvVKTtrwlWJpzs4SX1tk">
<meta name="robots" content="max-image-preview:large">
<meta name="twitter:card" content="summary">
<meta property="og:title" content="Thank You | PIM ARCHITECTS">
<meta property="og:type" content="website">
<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
<script>var currDev=function(){var c=window.getComputedStyle(document.getElementsByClassName('device')[0]).getPropertyValue('background-color');return(c!=undefined?parseInt(c.substring(4)):0)}</script>
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
<link rel="apple-touch-icon" sizes="180x180" href="images/apple-touch-icon-93bcce.png">
<meta name="msapplication-TileImage" content="images/mstile-144x144-c1fe60.png">
<link rel="manifest" href="manifest.json" crossOrigin="use-credentials">
<link rel="mask-icon" href="website-icon-b04c2c.svg" color="rgb(0,0,0)">
<script>window.gaf=function(){var e,t,a,n;e="script",t=document,a=t.createElement(e),n=t.getElementsByTagName(e)[0],a.async=1,a.src="https://www.googletagmanager.com/gtag/js?id=UA-4705414-41",n.parentNode.insertBefore(a,n)};if(!!document.cookie.match(/cookieConsent=true/))window.gaf();</script><script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js',new Date());gtag('config','UA-4705414-41',{'anonymize_ip':true,'forceSSL':true});</script>
<link rel="alternate" hreflang="en" href="https://pimarchitects.com/thank-you-for-contacting-us.php">
<link rel="stylesheet" href="css/site.b04c2c.css" type="text/css">
</head>
<body id="b1">
<div class="ps73 v1 s83">
<div class="v2 ps74 s84 c2">
<div class="v2 ps75 s85 w1">
<div class="v2 ps3 s2 c97">
<div class="v2 ps3 s2 c2">
<div class="v2 ps3 s3 c84">
<a href="/" class="a1"><picture class="i2"><source srcset="images/pim-logo-82-1.png 1x, images/pim-logo-164-1.png 2x" media="(max-width:479px)"><source srcset="images/pim-logo-123-1.png 1x, images/pim-logo-246-1.png 2x" media="(max-width:767px)"><source srcset="images/pim-logo-68-1.png 1x, images/pim-logo-136-1.png 2x" media="(max-width:959px)"><source srcset="images/pim-logo-85-1.png 1x, images/pim-logo-170-1.png 2x" media="(max-width:1199px)"><source srcset="images/pim-logo-106-1.png 1x, images/pim-logo-212-1.png 2x" media="(min-width:1200px)"><img src="images/pim-logo-212-1.png" class="un257 i1"></picture></a>
</div>
<div id="menu" class="v3 ps4 s4 c85">
<p class="p1 f1"><a href="about-us.php">about us</a><span class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="f1"><a href="projects.php">projects</a></span><span class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="f1"><a href="/">loop</a></span><span class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="f1"><a href="references.php">references</a></span><span class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="f1"><a href="contact.php">contact</a></span></p>
</div>
</div>
</div>
<div class="v4 ps76 s17 c49">
<ul class="menu-dropdown-1 v5 ps3 s18 m1" id="m9">
<li class="v2 ps3 s18 mit1">
<div class="menu-content mcv1">
<div class="v2 ps3 s19 c98">
<div class="v2 ps14 s20 c13">
<p class="p2 f4">Menu</p>
</div>
</div>
</div>
<ul class="menu-dropdown v6 ps15 s21 m1" id="m10">
<li class="v2 ps3 s22 mit1">
<a href="/" class="ml1"><div class="menu-content mcv2"><div class="v2 ps3 s23 c99"><div class="v2 ps16 s24 c15"><p class="p2 f5">Home</p></div></div></div></a>
</li>
<li class="v2 ps17 s22 mit1">
<a href="about-us.php" class="ml1"><div class="menu-content mcv2"><div class="v2 ps3 s23 c100"><div class="v2 ps16 s24 c15"><p class="p2 f5">About Us</p></div></div></div></a>
</li>
<li class="v5 ps18 s22 mit1">
<a href="projects.php" class="ml1"><div class="menu-content mcv2"><div class="v2 ps3 s23 c101"><div class="v2 ps16 s24 c15"><p class="p2 f5">Projects</p></div></div></div></a>
</li>
<li class="v5 ps18 s22 mit1">
<a href="references.php" class="ml1"><div class="menu-content mcv2"><div class="v2 ps3 s23 c102"><div class="v2 ps16 s24 c15"><p class="p2 f5">References</p></div></div></div></a>
</li>
<li class="v5 ps18 s22 mit1">
<a href="contact.php" class="ml1"><div class="menu-content mcv2"><div class="v2 ps3 s23 c103"><div class="v2 ps16 s24 c15"><p class="p2 f5">Contact</p></div></div></div></a>
</li>
</ul>
</li>
</ul>
</div>
<div class="v2 ps77 s86 c104">
<p class="p3 f13">Thank you for <span class="f14">contacting us</span></p>
</div>
<div class="v2 ps78 s57 c94">
<div class="v2 ps3 s57 c2">
<div class="v2 ps3 s58 c105">
<p class="p3 f6">Co-11 Ye&scedil;ilce Mh. Dalg&imath;&ccedil; Sok. No.11 Kat 1 Ka&gbreve;&imath;thane &Idot;stanbul</p>
</div>
<div class="v2 ps50 s59 c106">
<p class="p3 f6"><a href="tel:+902129444528">+90 212 944 45 28</a></p>
</div>
<div class="v2 ps51 s32 c58">
<div class="v2 ps3 s32 c2">
<div class="v2 ps28 s63 c3">
<a href="<?php echo $site_info["facebook"] ?>" target="_blank" rel="noopener" class="a1"><picture id="social" class="i2"><source srcset="images/facebook-2-31.png 1x, images/facebook-2-62.png 2x" media="(max-width:479px)"><source srcset="images/facebook-2-46.png 1x, images/facebook-2-92.png 2x" media="(max-width:767px)"><source srcset="images/facebook-2-15.png 1x, images/facebook-2-30.png 2x" media="(max-width:959px)"><source srcset="images/facebook-2-20.png 1x, images/facebook-2-40.png 2x" media="(max-width:1199px)"><source srcset="images/facebook-2-24.png 1x, images/facebook-2-48.png 2x" media="(min-width:1200px)"><img src="images/facebook-2-48.png" class="un258 i11"></picture></a>
</div>
<div class="v2 ps29 s73 c82">
<a href="<?php echo $site_info["linkedin"] ?>" target="_blank" rel="noopener" class="a1"><picture id="social" class="i2"><source srcset="images/linkedin-31-1.png 1x, images/linkedin-62-1.png 2x" media="(max-width:479px)"><source srcset="images/linkedin-46-1.png 1x, images/linkedin-92-1.png 2x" media="(max-width:767px)"><source srcset="images/linkedin-15.png 1x, images/linkedin-30.png 2x" media="(max-width:959px)"><source srcset="images/linkedin-20-1.png 1x, images/linkedin-40-1.png 2x" media="(max-width:1199px)"><source srcset="images/linkedin-24-1.png 1x, images/linkedin-48-1.png 2x" media="(min-width:1200px)"><img src="images/linkedin-48-1.png" class="un259 i12"></picture></a>
</div>
<div class="v2 ps30 s61 c107">
<a href="<?php echo $site_info["instagram"] ?>" target="_blank" rel="noopener" class="a1"><picture id="social" class="i2"><source srcset="images/instagram-31-1.png 1x, images/instagram-62-1.png 2x" media="(max-width:479px)"><source srcset="images/instagram-46-1.png 1x, images/instagram-92-1.png 2x" media="(max-width:767px)"><source srcset="images/instagram-15.png 1x, images/instagram-30.png 2x" media="(max-width:959px)"><source srcset="images/instagram-20-1.png 1x, images/instagram-40-1.png 2x" media="(max-width:1199px)"><source srcset="images/instagram-24-1.png 1x, images/instagram-48-1.png 2x" media="(min-width:1200px)"><img src="images/instagram-48-1.png" class="un260 i11"></picture></a>
</div>
<div class="v2 ps31 s36 c25">
<a href="javascript:em1();" class="a1"><picture id="social" class="i2"><source srcset="images/email-37-1.png 1x, images/email-74-1.png 2x" media="(max-width:479px)"><source srcset="images/email-55-1.png 1x, images/email-110-1.png 2x" media="(max-width:767px)"><source srcset="images/email-19.png 1x, images/email-38.png 2x" media="(max-width:959px)"><source srcset="images/email-23-1.png 1x, images/email-46-1.png 2x" media="(max-width:1199px)"><source srcset="images/email-29-1.png 1x, images/email-58-1.png 2x" media="(min-width:1200px)"><img src="images/email-58-1.png" class="un261 i8"></picture></a>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="v2 ps79 s87 w1">
<div class="v2 ps3 s88 c108">
<picture class="i2">
<source srcset="images/construction-helmet-320-1.png 1x, images/construction-helmet-640-1.png 2x" media="(max-width:479px)">
<source srcset="images/construction-helmet-480-1.png 1x, images/construction-helmet-960-1.png 2x" media="(max-width:767px)">
<source srcset="images/construction-helmet-486-2.png 1x, images/construction-helmet-972-2.png 2x" media="(max-width:959px)">
<source srcset="images/construction-helmet-608-2.png 1x, images/construction-helmet-1216-2.png 2x" media="(max-width:1199px)">
<source srcset="images/construction-helmet-760-2.png 1x, images/construction-helmet-1520-2.png 2x" media="(min-width:1200px)">
<img src="images/construction-helmet-1520-2.png" class="un262 i18">
</picture>
</div>
<div class="v2 ps80 s28 c9">
<div class="wrapper6">
<style>
#menu a{
}
#menu a:hover{
padding-bottom:5px;
border-bottom: 5px solid #000000;
}
#references{
-webkit-filter: grayscale(100%);
filter: grayscale(100%);
transition: all 0.5s ease;
}
#references:hover{
-webkit-filter: grayscale(0%);
filter: grayscale(0%);
transition: all 0.2s ease;
cursor: pointer;
transform: scale(1.2) rotate(-2deg);
box-shadow:0px 0px 50px #ccc;
}
#vitrin{
opacity:1;
transition: all 0.2s ease;
}
#vitrin:hover{
opacity:0;
cursor: pointer;
transition: all 0.5s ease;
transform:scale(0.9);
cursor: pointer;

}
#port{
transition: all 0.5s ease;
transform:scale(1);
box-shadow:0px 0px 0px #ccc;
}
#port:hover{
cursor: pointer;
transition: all 0.5s ease;
transform:scale(0.9);
box-shadow:0px 0px 50px #999;
}
#social{
opacity:0.95;
transition: all 0.5s ease;
}
#social:hover{
opacity:1;
transition: all 0.5s ease;
}

</style>
</div>
</div>
</div>
</div>
</div>
<div id="consentBanner" class="v6 ps32 s37 c109">
<div class="ps33 v1 s38">
<div class="v2 ps34 s39 c35">
<a href="javascript:void(0)" class="allowConsent f7 btn5 v7 s40">OK</a>
</div>
<div class="v2 ps35 s41 c36">
<p class="p3 f8">This website makes use of cookies. Please see our <span class="f9"><a class="noConsent" href="privacy-policy.html">privacy policy</a></span> for details.</p>
</div>
</div>
</div>
<div class="c37">
</div>
<div class="device"></div>
<script>var p=document.createElement("P");p.innerHTML="&nbsp;",p.style.cssText="position:fixed;visible:hidden;font-size:100px;zoom:1",document.body.appendChild(p);var rsz=function(e){return function(){var r=Math.trunc(1e3/parseFloat(window.getComputedStyle(e).getPropertyValue("font-size")))/10,t=document.body;r!=t.style.getPropertyValue("--f")&&t.style.setProperty("--f",r)}}(p);if("ResizeObserver"in window){var ro=new ResizeObserver(rsz);ro.observe(p)}else if("requestAnimationFrame"in window){var raf=function(){rsz(),requestAnimationFrame(raf)};requestAnimationFrame(raf)}else setInterval(rsz,100);</script>

<script>dpth="/"</script>
<script type="text/javascript" src="js/jquery.a5d7fb.js"></script>
<script type="text/javascript" src="js/jqueryui.a5d7fb.js"></script>
<script type="text/javascript" src="js/menu.a5d7fb.js"></script>
<script type="text/javascript" src="js/menu-dropdown-animations.a5d7fb.js"></script>
<script type="text/javascript" src="js/menu-dropdown-1.b04c2c.js"></script>
<script type="text/javascript" src="js/menu-dropdown.b04c2c.js"></script>
<script type="text/javascript" src="js/consent.a5d7fb.js"></script>
<script type="text/javascript" src="js/thank-you-for-contacting-us.b04c2c.js"></script>
</body>
</html>