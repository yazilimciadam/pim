<?php 

ini_set("upload_tmp_dir", "/tmp");
error_reporting(E_ALL);
ini_set('display_errors', 1);

class MainClass  
{
    
   
    function __construct()
    {
        
       
        $this->db_name = "pim_agency";
        $this->db_user = "root";
        $this->db_pass = "rootpassword";
        $this->db_host = "mysql_db_container";
        try {
            $this->connectionDb = new PDO("mysql:host=$this->db_host;dbname=$this->db_name;charset=UTF8", $this->db_user, $this->db_pass,array(PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
            
        } catch (PDOException $th) {
            echo "Error: " . $th->getMessage();
        }
    }

    public function sessionControl (){
        if(!isset($_SESSION['user'])){
            header("Location: login.php");
        }
    }

   public function getAbout (){
        $sql = "SELECT * FROM about WHERE about_id = 1";
        $result = $this->connectionDb->query($sql);
        $row = $result->fetch(PDO::FETCH_ASSOC);
       
        return $row;
    }

    public function getPersona (){
        $query = "SELECT * FROM about_personal ";
        $result = $this->connectionDb->query($query);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;

    }

    public function getProjects ($id){

        $query = "SELECT * FROM projects WHERE project_id=$id";
        $result = $this->connectionDb->query($query);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function getAllReferences (){

        $query = "SELECT * FROM references_pim ORDER BY sortId ASC";
        $result = $this->connectionDb->query($query);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;

    }

    public function getAllProjects (){

        $query = "SELECT * FROM projects ORDER BY isSortId ASC";
        $result = $this->connectionDb->query($query);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;

    }

    public function deleteReference ($id){
            
            $query = "DELETE FROM references_pim WHERE ref_id = $id";
            $result = $this->connectionDb->query($query);
            return $result;
    }

    public function deleteProject ($id){
            
        $query = "DELETE FROM projects WHERE project_id = $id";
        $result = $this->connectionDb->query($query);
        return $result;
}

    public function getAllProjectsHome (){

        $query = "SELECT * FROM projects WHERE isHomePage = 1 ORDER BY isHomePageSort ASC";
        $result = $this->connectionDb->query($query);
        $row = $result->fetchAll(PDO::FETCH_ASSOC);
        return $row;

    }

    public function updateHomePage ($id, $isHomePage){

        $query = "UPDATE projects SET isHomePage = '$isHomePage' WHERE project_id = $id";
        $result = $this->connectionDb->query($query);
        return $result;
    }
    public function updateHomePageSort ($array){
        $var = [];
        foreach ($array as $key => $value) {
            $query = "UPDATE projects SET isHomePageSort = '$key' WHERE project_id = $value";
            $result = $this->connectionDb->query($query);
            array_push($var,$this->connectionDb->lastInsertId());
        }
        return $var;
    }

        public function updateSortProject ($array){
        $var = [];
        foreach ($array as $key => $value) {
            $query = "UPDATE projects SET isSortId = '$key' WHERE project_id = $value";
            $result = $this->connectionDb->query($query);
            array_push($var,$this->connectionDb->lastInsertId());
        }
        return $var;
    }

    public function addProject ($title, $area, $location, $images,$banner, $thumb){

        $query = "INSERT INTO projects (project_title, project_area, project_location, project_images, project_banner,thumbnail) VALUES ('$title', '$area', '$location', '$images', '$banner', '$thumb')";
        $result = $this->connectionDb->query($query);
        $var = $this->connectionDb->lastInsertId();
        return $var;


    }

    public function updateProject ($id, $title, $area, $location, $images,$banner, $thumb){

        $query = "UPDATE projects SET project_title = '$title', project_area = '$area', project_location = '$location', project_images = '$images', project_banner = '$banner', thumbnail = '$thumb' WHERE project_id = $id";
        $result = $this->connectionDb->query($query);
        return $result;
    }


    public function getPersonaID ($id) {
        $query = "SELECT * FROM about_personal WHERE per_id = $id";
        $result = $this->connectionDb->query($query);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

    public function updatePersona ($id, $name, $about, $image){

        $query = "UPDATE about_personal SET name = '$name', text = '$about', image = '$image' WHERE per_id = $id";
        $result = $this->connectionDb->query($query);
        return $result;

    }

    public function deletePersona ($id){
            
            $query = "DELETE FROM about_personal WHERE per_id = $id";
            $result = $this->connectionDb->query($query);
            return $result;
    }

    public function addReference ($image){
            
            $query = "INSERT INTO references_pim (image) VALUES ('$image')";
            $result = $this->connectionDb->query($query);
            $var = $this->connectionDb->lastInsertId();
            return $var;
    }

    public function updateSortReference ($array){
        $var = [];
        foreach ($array as $key => $value) {
            $query = "UPDATE references_pim SET sortId = '$key' WHERE ref_id = $value";
            $result = $this->connectionDb->query($query);
            array_push($var,$this->connectionDb->lastInsertId());
        }
        return $var;
    } 
    public function addPersonal ($name, $text, $image){
            
        $query = "INSERT INTO about_personal (name, text, image) VALUES ('$name', '$text', '$image')";
        $result = $this->connectionDb->query($query);
        $var = $this->connectionDb->lastInsertId();
        return $var;
    }
   
    public function UpdateAbout ($about, $facebook, $linkedin, $instagram,$address){

        $query = "UPDATE about SET about = '$about', facebook = '$facebook', linkedin = '$linkedin', instagram = '$instagram', address= '$address' WHERE about_id = 1";
        $result = $this->connectionDb->query($query);
        return $result;


    }

    public function login ($mail,$pass){
        $query = "SELECT * FROM users WHERE mail = '$mail' AND password = md5('$pass') ";
        $result = $this->connectionDb->query($query);
        $row = $result->fetch(PDO::FETCH_ASSOC);
        return $row;
    }

}
