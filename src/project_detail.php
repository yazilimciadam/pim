<?php 
include "classes/MainClass.php";
$veritabani = new MainClass();
$project = $veritabani->getProjects($_GET['id']);
$images = json_decode($project['project_images'], true);

$site_info = $veritabani->getAbout(1);


?>

<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

<head>
    <meta charset="UTF-8">
    <title><?php echo $project["project_title"] ?> | PIM ARCHITECTS</title>
    <meta name="referrer" content="same-origin">
    <link rel="canonical" href="https://pimarchitects.com/king-home-appliances-office-pim-architects.html">
    <meta name="google-site-verification" content="VCnOnu8apZKI4ETt2bXQiNohvVKTtrwlWJpzs4SX1tk">
    <meta name="robots" content="max-image-preview:large">
    <meta name="twitter:card" content="summary">
    <meta property="og:title" content="KING HOME APPLIANCES OFFICE | PIM ARCHITECTS">
    <meta property="og:type" content="website">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <script>
        var currDev = function() {
            var c = window.getComputedStyle(document.getElementsByClassName('device')[0]).getPropertyValue('background-color');
            return (c != undefined ? parseInt(c.substring(4)) : 0)
        }
    </script>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link rel="apple-touch-icon" sizes="180x180" href="images/apple-touch-icon-93bcce.png">
    <meta name="msapplication-TileImage" content="images/mstile-144x144-c1fe60.png">
    <link rel="manifest" href="manifest.json" crossOrigin="use-credentials">
    <link rel="mask-icon" href="website-icon-b04c2c.svg" color="rgb(0,0,0)">
    <script>
        window.gaf = function() {
            var e, t, a, n;
            e = "script", t = document, a = t.createElement(e), n = t.getElementsByTagName(e)[0], a.async = 1, a.src = "https://www.googletagmanager.com/gtag/js?id=UA-4705414-41", n.parentNode.insertBefore(a, n)
        };
        if (!!document.cookie.match(/cookieConsent=true/)) window.gaf();
    </script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-4705414-41', {
            'anonymize_ip': true,
            'forceSSL': true
        });
    </script>
    <link rel="alternate" hreflang="en" href="https://pimarchitects.com/king-home-appliances-office-pim-architects.html">
    <link rel="stylesheet" href="css/site.b04c2c.css" type="text/css">
</head>

<body id="b1">
    <div class="ps1 v1 s108">
        <div class="v2 ps2 s109 c2">
            <div class="v2 ps3 s110 w1">
                <div class="v2 ps3 s111 c38">
                    <div class="v2 ps3 s111 c2">
                        <div class="v2 ps3 s3 c9">
                            <a href="/" class="a1">
                                <picture class="i2">
                                    <source srcset="images/pim-logo-82-1.png 1x, images/pim-logo-164-1.png 2x" media="(max-width:479px)">
                                    <source srcset="images/pim-logo-123-1.png 1x, images/pim-logo-246-1.png 2x" media="(max-width:767px)">
                                    <source srcset="images/pim-logo-68-1.png 1x, images/pim-logo-136-1.png 2x" media="(max-width:959px)">
                                    <source srcset="images/pim-logo-85-1.png 1x, images/pim-logo-170-1.png 2x" media="(max-width:1199px)">
                                    <source srcset="images/pim-logo-106-1.png 1x, images/pim-logo-212-1.png 2x" media="(min-width:1200px)"><img src="images/pim-logo-212-1.png" class="un331 i1">
                                </picture>
                            </a>
                        </div>
                        <div id="menu" class="v3 ps4 s112 c130">
                            <p class="p1 f1"><a href="about-us.php">about us</a><span class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="f1"><a href="projects.php">projects</a></span><span class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="f1"><a href="/">loop</a></span><span class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="f1"><a href="references.php">references</a></span><span class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="f1"><a href="contact.php">contact</a></span><span class="f1">&nbsp;</span></p>
                        </div>
                    </div>
                </div>
                <div class="v4 ps76 s17 c131">
                    <ul class="menu-dropdown-1 v5 ps3 s18 m1" id="m13">
                        <li class="v2 ps3 s18 mit1">
                            <div class="menu-content mcv1">
                                <div class="v2 ps3 s19 c132">
                                    <div class="v2 ps14 s20 c13">
                                        <p class="p2 f4">Menu</p>
                                    </div>
                                </div>
                            </div>
                            <ul class="menu-dropdown v6 ps15 s53 m1" id="m14">
                                <li class="v2 ps3 s54 mit1">
                                    <a href="/" class="ml1">
                                        <div class="menu-content mcv2">
                                            <div class="v2 ps3 s55 c133">
                                                <div class="v2 ps16 s56 c15">
                                                    <p class="p2 f5">Home</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="v2 ps17 s54 mit1">
                                    <a href="about-us.php" class="ml1">
                                        <div class="menu-content mcv2">
                                            <div class="v2 ps3 s55 c134">
                                                <div class="v2 ps16 s56 c15">
                                                    <p class="p2 f5">About Us</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="v5 ps45 s54 mit1">
                                    <a href="projects.php" class="ml1">
                                        <div class="menu-content mcv2">
                                            <div class="v2 ps3 s55 c135">
                                                <div class="v2 ps16 s56 c15">
                                                    <p class="p2 f5">Projects</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="v2 ps17 s54 mit1">
                                    <a href="#" class="ml1">
                                        <div class="menu-content mcv2">
                                            <div class="v2 ps3 s55 c136">
                                                <div class="v2 ps16 s56 c15">
                                                    <p class="p2 f5">Loop</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="v5 ps45 s54 mit1">
                                    <a href="references.php" class="ml1">
                                        <div class="menu-content mcv2">
                                            <div class="v2 ps3 s55 c137">
                                                <div class="v2 ps16 s56 c15">
                                                    <p class="p2 f5">References</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="v5 ps45 s54 mit1">
                                    <a href="contact.php" class="ml1">
                                        <div class="menu-content mcv2">
                                            <div class="v2 ps3 s55 c138">
                                                <div class="v2 ps16 s56 c15">
                                                    <p class="p2 f5">Contact</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="v2 ps100 s113 c2">
                    <div class="v2 ps101 s114 c139">
                        <div class="v2 ps3 s115 c2">
                            <div class="v2 ps3 s115 w1">
                                <div class="v2 ps102 s116 c140">
                                    <p class="p4 f21">
                                        <?php
                                        $ss = explode(" ",$project["project_title"]);
                                        $b = count($ss);
                                        $a = $b-1;
                                        for($i=0;$i<$a;$i++){
                                            echo $ss[$i]." ";
                                        }
                                        ?>
                                    </p>
                                    <p class="p4 f22"><?php 
                                    
                                    $ss = explode(" ",$project["project_title"]); 
                                    $b = count($ss);
                                    $a = $b-1;
                                    echo $ss[$a];

                                    
                                    
                                    ?></p>
                                    
                                </div>
                                <div class="v2 ps103 s117 c141"></div>
                            </div>
                        </div>
                        <div class="v2 ps104 s118 c142">
                            <p class="p6 f23">Project Name<br><span class="f24"><?php echo $project["project_title"]; ?></span></p>
                            <p class="p6 f23">Project Location<br><span class="f24"> <?php echo $project["project_location"] ?> </span></p>
                            <p class="p6 f23">Total Construction Area<br><span class="f24"><?php echo $project["project_area"] ?></span></p>
                        </div>
                    </div>
                    <div class="v2 ps105 s119 w1">
                        <?php var_dump((count($images)>=2)?count($images):2) ?>
                        <?php echo count($images) ?>
                        <?php 
                        if(count($images)>2){
                            $a=2;
                        }else{
                            $a=count($images);
                        }
                        for ($i=0; $i <$a; $i++) { 
                            $elementIm = $images[$i];
                            ?>
                            <div <?php echo ($i==0)? 'style="margin-bottom: 20px; height:auto;"':""  ?> class="v2 ps3 s120 c24">
                            <a  href='javascript:acMagnific("<?php echo $elementIm["image"] ?>");'  data-mfp-src="<?php echo $elementIm["image"] ?>" class="a1 un398">
                                <picture class="i2">
                                   <img src="<?php echo $elementIm["image"] ?>"  onclick='acMagnific("<?php echo $elementIm["image"] ?>")' style="height:auto;position:inherit;"  class="un334 i23">
                                </picture>
                            </a>
                        </div>
                        <?php } ?>
                        
                        
                    </div>
                </div>
            </div>
            <div class="v2 ps106 s28 c81">
                <div class="wrapper9">
                    <style>
                        #menu a {}

                        #menu a:hover {
                            padding-bottom: 5px;
                            border-bottom: 5px solid #000000;
                        }

                        #references {
                            -webkit-filter: grayscale(100%);
                            filter: grayscale(100%);
                            transition: all 0.5s ease;
                        }

                        #references:hover {
                            -webkit-filter: grayscale(0%);
                            filter: grayscale(0%);
                            transition: all 0.2s ease;
                            cursor: pointer;
                            transform: scale(1.2) rotate(-2deg);
                            box-shadow: 0px 0px 50px #ccc;
                        }

                        #vitrin {
                            opacity: 1;
                            transition: all 0.2s ease;
                        }

                        #vitrin:hover {
                            opacity: 0;
                            cursor: pointer;
                            transition: all 0.5s ease;
                            transform: scale(0.9);
                            cursor: pointer;

                        }

                        #port {
                            transition: all 0.5s ease;
                            transform: scale(1);
                            box-shadow: 0px 0px 0px #ccc;
                        }

                        #port:hover {
                            cursor: pointer;
                            transition: all 0.5s ease;
                            transform: scale(0.9);
                            box-shadow: 0px 0px 50px #999;
                        }

                        #social {
                            opacity: 0.95;
                            transition: all 0.5s ease;
                        }

                        #social:hover {
                            opacity: 1;
                            transition: all 0.5s ease;
                        }
                    </style>
                </div>
            </div>
        </div>
        <?php if(count($images)<3){

        }else{ ?>
        <?php for ($i=0; $i < count($images); $i++) { 
                $elementImage = $images[$i];
                if($i>1){

                
                //var_dump($elementImage);
             ?>
        <div class="v2 ps107 s122 c3">
           
        <a  href='javascript:acMagnific("<?php echo $elementImage["image"] ?>");'  data-mfp-src="<?php echo $elementImage["image"] ?>"  class="a1 un398">
                <picture class="i2">
                   <img src="<?php echo $elementImage["image"] ?>" onclick='acMagnific("<?php echo $elementImage["image"] ?>")' class="un334 i23">
                </picture>
            </a>
         
        </div>
        <?php } }} ?>
        <div class="v2 ps110 s127 c2">
            <div class="v2 ps111 s128 c122">
                <div class="v2 ps3 s128 c2">
                    <div class="v2 ps112 s129 c31">
                        <a href="<?php echo $site_info["facebook"] ?>" target="_blank" rel="noopener" class="a1">
                            <picture id="social" class="i2 un407">
                                <source data-srcset="images/facebook-2-31.png 1x, images/facebook-2-62.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(max-width:479px)">
                                <source data-srcset="images/facebook-2-46.png 1x, images/facebook-2-92.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(max-width:767px)">
                                <source data-srcset="images/facebook-2-15.png 1x, images/facebook-2-30.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(max-width:959px)">
                                <source data-srcset="images/facebook-2-19.png 1x, images/facebook-2-38.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(max-width:1199px)">
                                <source data-srcset="images/facebook-2-24.png 1x, images/facebook-2-48.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(min-width:1200px)"><img src="images/facebook-2-48.png" class="un339 i16">
                            </picture>
                        </a>
                    </div>
                    <div class="v2 ps113 s129 c32">
                        <a href="<?php echo $site_info["linkedin"] ?>" target="_blank" rel="noopener" class="a1">
                            <picture id="social" class="i2 un408">
                                <source data-srcset="images/linkedin-31-1.png 1x, images/linkedin-62-1.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(max-width:479px)">
                                <source data-srcset="images/linkedin-46-2.png 1x, images/linkedin-92-2.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(max-width:767px)">
                                <source data-srcset="images/linkedin-15-1.png 1x, images/linkedin-30-1.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(max-width:959px)">
                                <source data-srcset="images/linkedin-19-1.png 1x, images/linkedin-38-1.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(max-width:1199px)">
                                <source data-srcset="images/linkedin-24-1.png 1x, images/linkedin-48-1.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(min-width:1200px)"><img src="images/linkedin-48-1.png" class="un340 i16">
                            </picture>
                        </a>
                    </div>
                    <div class="v2 ps114 s130 c33">
                        <a href="<?php echo $site_info["instagram"] ?>" target="_blank" rel="noopener" class="a1">
                            <picture id="social" class="i2 un409">
                                <source data-srcset="images/instagram-31-1.png 1x, images/instagram-62-1.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(max-width:479px)">
                                <source data-srcset="images/instagram-46-1.png 1x, images/instagram-92-1.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(max-width:767px)">
                                <source data-srcset="images/instagram-15-1.png 1x, images/instagram-30-1.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(max-width:959px)">
                                <source data-srcset="images/instagram-19-1.png 1x, images/instagram-38-1.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(max-width:1199px)">
                                <source data-srcset="images/instagram-24-1.png 1x, images/instagram-48-1.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(min-width:1200px)"><img src="images/instagram-48-1.png" class="un341 i17">
                            </picture>
                        </a>
                    </div>
                    <div class="v2 ps115 s131 c143">
                        <a href="javascript:em1();" class="a1">
                            <picture id="social" class="i2 un410">
                                <source data-srcset="images/email-37-1.png 1x, images/email-74-1.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(max-width:479px)">
                                <source data-srcset="images/email-56-1.png 1x, images/email-112-1.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(max-width:767px)">
                                <source data-srcset="images/email-18.png 1x, images/email-36.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(max-width:959px)">
                                <source data-srcset="images/email-24-1.png 1x, images/email-48-1.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(max-width:1199px)">
                                <source data-srcset="images/email-29-2.png 1x, images/email-58-2.png 2x" srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" media="(min-width:1200px)"><img src="images/email-58-2.png" class="un342 i28">
                            </picture>
                        </a>
                    </div>
                </div>
            </div>
          
        </div>
    </div>
    <div id="consentBanner" class="v6 ps32 s37 c144">
        <div class="ps33 v1 s38">
            <div class="v2 ps34 s39 c35">
                <a href="javascript:void(0)" class="allowConsent f7 btn10 v7 s40">OK</a>
            </div>
            <div class="v2 ps35 s41 c36">
                <p class="p3 f8">This website makes use of cookies. Please see our <span class="f9"><a class="noConsent" href="privacy-policy.html">privacy policy</a></span> for details.</p>
            </div>
        </div>
    </div>
    <div class="c37">
    </div>
    <div class="device"></div>
    <script>
        var p = document.createElement("P");
        p.innerHTML = "&nbsp;", p.style.cssText = "position:fixed;visible:hidden;font-size:100px;zoom:1", document.body.appendChild(p);
        var rsz = function(e) {
            return function() {
                var r = Math.trunc(1e3 / parseFloat(window.getComputedStyle(e).getPropertyValue("font-size"))) / 10,
                    t = document.body;
                r != t.style.getPropertyValue("--f") && t.style.setProperty("--f", r)
            }
        }(p);
        if ("ResizeObserver" in window) {
            var ro = new ResizeObserver(rsz);
            ro.observe(p)
        } else if ("requestAnimationFrame" in window) {
            var raf = function() {
                rsz(), requestAnimationFrame(raf)
            };
            requestAnimationFrame(raf)
        } else setInterval(rsz, 100);
    </script>

    <script>
        dpth = "/"
    </script>
    <script type="text/javascript" src="js/woolite.bfc084 2.js"></script>
    <script type="text/javascript" src="js/jquery.a5d7fb.js"></script>
    <script type="text/javascript" src="js/jqueryui.a5d7fb.js"></script>
    <script type="text/javascript" src="js/menu.a5d7fb.js"></script>
    <script type="text/javascript" src="js/menu-dropdown-animations.a5d7fb.js"></script>
    <script type="text/javascript" src="js/menu-dropdown-1.b04c2c.js"></script>
    <script type="text/javascript" src="js/menu-dropdown.b04c2c.js"></script>
    <script type="text/javascript" src="js/lightbox.a5d7fb.js"></script>
    <script type="text/javascript" src="js/consent.a5d7fb.js"></script>
    <script type="text/javascript" src="js/king-home-appliances-office-pim-architects.b04c2c.js"></script>
</body>

</html>