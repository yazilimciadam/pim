<?php

if (isset($_POST["mail"])) {
  @ob_start();
session_start();
  include "../classes/MainClass.php";
  $veritabani = new MainClass();
  $response = $veritabani->login($_POST["mail"], $_POST["pass"]);

  if ($response) {
    $_SESSION["user"] = $response;
    header("Location: index.php");
    
  }
} 


?>

<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
  

    <title>PIM ADMIN Login</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/login.css" rel="stylesheet">
  </head>

  <body class="text-center">
    <form action="" method="POST">
     
      <br>
      <br>
      <img class="mb-4" src="../images/pim-logo-212-1.png">

      <br><br>

      <div class="form-floating">
      <label for="floatingInput" style="float:left;">Email address</label>
        <input type="email" class="form-control" id="floatingInput" name="mail" placeholder="name@example.com">
       
      </div>
      <br>
      <div class="form-floating">
      <label for="floatingPassword" style="float:left;">Password</label>
        <input type="password" class="form-control" id="floatingPassword" name="pass" placeholder="Password">
       
      </div>
      <br>


      <button class="w-100 btn btn-lg btn-primary" type="submit">Sign in</button>


    </form>
 
  </body>
</html>
