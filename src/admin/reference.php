<?php
session_start();
ob_start();
include "../classes/MainClass.php";
include("../classes/uploadImage/class.upload.php");
$veritabani = new MainClass();
$veritabani->sessionControl();

include('layout/header.php');


if (isset($_POST['category']) && ($_POST['category'] == "sorting")) {

    echo $veritabani->updateSortReference($_POST["data"]);
    
    die();
 }
 

if (isset($_POST['category']) && ($_POST['category'] == "reference_remove")) {

    $veritabani->deleteReference($_POST["reference_id"]);
}


if (isset($_POST['category']) && ($_POST['category'] == "reference_add")) {

    $imageBanner;
    $handle = new \Verot\Upload\Upload($_FILES["banner_image"], 'tr_TR');
    if ($handle->uploaded) {
        $handle->file_new_name_body   = time();
        $handle->image_resize         = true;
        $handle->image_ratio_crop = "T";
        $handle->image_x              = 350;
        $handle->image_y              = 238;

        $handle->file_src_pathname    = $_FILES["banner_image"]["tmp_name"];
        $handle->process("../images/");
        if ($handle->processed) {
            $imageBanner =  "images/" . $handle->file_dst_name;
        } else {
            echo 'error : ' . $handle->error;
        }
    }

    $vv =  $veritabani->addReference($imageBanner);
    if ($vv) {
        echo "<script>alert('Kayıt Başarılı');</script>";
        echo "<script>window.location.href='reference.php';</script>";
    }
}

?>

<br>
<br>
<div class="row">



    <div class="col-md-12">

        <h2>Reference Add</h2>
        <form enctype="multipart/form-data" method="post" action="reference.php">


            <div class="form-group">
                <label for="exampleInputEmail1">Reference Banner</label>
                <input type="file" class="form-control" name="banner_image" placeholder="Reference Banner">

            </div>
            <br>

            <input type="hidden" name="category" value="reference_add">
            <button type="submit" class="btn btn-primary">Add Reference</button>
        </form>

        <h2> Referanslar </h2>

        <table class="table">
            <thead>
                <th>Reference</th>
                <th>#</th>
            </thead>

            <tbody class="sortable_list">
                <?php
                $references = $veritabani->getAllReferences();
                $indexForeach = 0;
                foreach ($references as $reference) {
                    echo "<tr sort_id='".$indexForeach."' id='".$reference["ref_id"]."' class='' style='background-color:#fff'>";
                    echo '<td> <img src="../images/move-icon.svg" style="height:30px;"> <img src="/' . $reference["image"] . '" width="100px"></td>';
                    echo '<td>';
                    echo '
            <form method="post" action="/admin/reference.php">
            <button type="submit" class="btn btn-danger">Remove</button>
            <input type="hidden" name="category" value="reference_remove">
            <input type="hidden" name="reference_id" value="' . $reference["ref_id"] . '">

            </form>
            </td>';
                    echo "</tr>";
                    $indexForeach++;
                }
                ?>
            </tbody>

        </table>
        <script>
             $(function() {
$( ".sortable_list" ).sortable({
    connectWith: ".connectedSortable",
    update: function (event, ui) {
        var sorted = $( ".sortable_list" );
        
        var data = sorted.sortable('toArray');
        $.post("reference.php", {data:data, category:"sorting"}, function(result){
            console.log(result);
        });
    },
    receive: function(event, ui) {
       console.log(ui);
    }         
}).disableSelection();
    

});
        </script>

    </div>



</div>

<?php include('layout/footer.php'); ?>