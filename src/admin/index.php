<?php
session_start();
ob_start();
include "../classes/MainClass.php";
include("../classes/uploadImage/class.upload.php");
$veritabani = new MainClass();
$veritabani->sessionControl();

include('layout/header.php');

if (isset($_POST['category']) && ($_POST['category'] == "project_home_sort")) {

   echo $veritabani->updateHomePageSort($_POST["data"]);
   
   die();
}


if (isset($_POST['category']) && ($_POST['category'] == "project_sort")) {

    echo $veritabani->updateSortProject($_POST["data"]);
    
    die();
 }

if (isset($_POST['category']) && ($_POST['category'] == "project_home")) {

    if($_POST["status"] != 1){
        $veritabani->updateHomePage($_POST["project_id"], 1);
    }else{
        $veritabani->updateHomePage($_POST["project_id"], 0);
    }
    
}

if (isset($_POST['category']) && ($_POST['category'] == "project_remove")) {

    $veritabani->deleteProject($_POST["project_id"]);
}


if (isset($_POST['category']) && ($_POST['category'] == "project_add")) {


    $imageBanner;
    $handle = new \Verot\Upload\Upload($_FILES["banner_image"], 'tr_TR');
    if ($handle->uploaded) {
        $handle->file_new_name_body   = time()*3122;

        $handle->file_src_pathname    = $_FILES["banner_image"]["tmp_name"];
        $handle->process("../images/");
        if ($handle->processed) {
            $imageBanner =  "images/" . $handle->file_dst_name;
        } else {
            echo 'error : ' . $handle->error;
        }
        $handle->clean();
    }
   
    $imagethum;
    $handle4 = new \Verot\Upload\Upload($_FILES["thumbna"], 'tr_TR');
    
        if ($handle4->uploaded) {
            $handle4->file_new_name_body   = "aba".time()*233;
            $handle4->image_resize         = true;
            
            $handle4->image_ratio_crop = "T";
            $handle4->image_x              = 250;
            $handle4->image_y              = 180;
            
            $handle4->process("../images/");
            
        if ($handle4->processed) {
            $imagethum =  "images/" . $handle4->file_dst_name;
            
            
        } else {
            echo 'error : ' . $handle4->error;
        }
    }
    
    $files = array();
    foreach ($_FILES['detail_images'] as $k => $l) {
        foreach ($l as $i => $v) {
            if (!array_key_exists($i, $files))
                $files[$i] = array();
            $files[$i][$k] = $v;
        }
    }

    foreach ($files as $file) {

        $handle3 = new \Verot\Upload\Upload($file, 'tr_TR');
        if ($handle3->uploaded) {
            $handle3->file_new_name_body   = time()*471;
            $handle3->file_src_pathname    = $file["tmp_name"];
            $handle3->image_ratio = true;
            $handle3->image_resize         = true;
            $handle3->image_x              = 600;
            $handle3->image_ratio_y        = true;
            $handle3->process("../images/");
            if ($handle3->processed) {
                $arr[] = array(
                    "image" => "images/" . $handle3->file_dst_name,
                );
               
          
            } else {
                echo 'error : ' . $handle3->error;
            }
        }
        $handle3->clean();
        unset($handle3);
    }
   
   
    $imagesDetailed = json_encode($arr);

    $vv =  $veritabani->addProject($_POST["title"], $_POST["area"], $_POST["location"], $imagesDetailed, $imageBanner, $imagethum);
    if($vv){
        echo "<script>alert('Kayıt Başarılı');</script>";
       echo "<script>window.location.href='index.php';</script>";
    }
}


?>

<br>
<br>
<div class="row">

    <div class="col-md-12">

        <h2>Projects</h2>
        <form enctype="multipart/form-data" method="post" action="index.php">

            <div class="form-group">
                <label for="exampleInputEmail1">Title</label>
                <input type="text" name="title" class="form-control" placeholder="Project Title">
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Location</label>
                <input type="text" name="location" class="form-control" placeholder="Project Location">
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Area</label>
                <input type="text" name="area" class="form-control" placeholder="Project Area m2">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Thumbnail</label>
                <input type="file" class="form-control" name="thumbna" placeholder="Project Home Banner">
                <small>Projeler sayfasında görünen küçük fotoğraf</small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Home Banner</label>
                <input type="file" class="form-control" name="banner_image" placeholder="Project Home Banner">
                <small>Anasayfada Görünür ayarlanırsa bu fotograf kullanılır.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Images</label>
                <input type="file" name="detail_images[]" multiple class="form-control" placeholder="Project Images">
                <small>Proje Detayındaki Fotograflar.</small>
            </div>
            <input type="hidden" name="category" value="project_add">
            <button type="submit" class="btn btn-primary">Add Project</button>
        
        </form>

        <h2>Projects Home Page</h2>

        <table class="table">
            <thead>
                <th>Project</th>
                <th>Location</th>
                <th>Area</th>
                <th>#</th>
                <th>#</th>
                
            </thead>
            <tbody class="sortable_list">
                <?php
                $projects = $veritabani->getAllProjectsHome();
                $indexForeach = 0;
                foreach ($projects as $project) {
                   
                   
                    echo "<tr sort_id='".$indexForeach."' id='".$project["project_id"]."' style='background-color:#ffff'>";
                   
               
                    echo "<td> <img src='../images/move-icon.svg' style='height:30px;'> " . $project["project_title"] . "</td>";
                    echo "<td>" . $project["project_location"] . "</td>";
                    echo "<td>" . $project["project_area"] . "</td>";
                    echo '<td>';

                    if($project["isHomePage"] == 1) { 
                    echo     '
                        <form method="post" action="/admin/index.php">
                        <button type="submit" class="btn btn-danger">Remove Home Page</button>
                        <input type="hidden" name="category" value="project_home">
                        <input type="hidden" name="project_id" value="' . $project["project_id"] . '">
                        <input type="hidden" name="status" value="' . $project["isHomePage"] . '">
                        </form>
                    ';
                    } else { 
                    echo '
                    <form method="post" action="/admin/index.php">
                    <button type="submit" class="btn btn-success">Add Home Page</button>
                    <input type="hidden" name="category" value="project_home">
                    <input type="hidden" name="project_id" value="' . $project["project_id"] . '">
                    <input type="hidden" name="status" value="' . $project["isHomePage"] . '">
                    </form>
                    </td>';
                    }
                    echo "<td>";
                    echo "<td>";
                    echo '
                    <a href="/admin/project_edit.php?id=' . $project["project_id"] . '" class="btn btn-primary">Edit</a>';
                    echo "</td>";
                    $indexForeach++;
                  
                 
                 
                    

                    echo "</tr>";
                    
                }
                ?>
            </tbody>
        </table>

        <script>
             $(function() {
$( ".sortable_list" ).sortable({
    connectWith: ".connectedSortable",
    update: function (event, ui) {
        var sorted = $( ".sortable_list" );
        
        var data = sorted.sortable('toArray');
        $.post("index.php", {data:data, category:"project_home_sort"}, function(result){
            console.log(result);
        });
    },
    receive: function(event, ui) {
       console.log(ui);
    }         
}).disableSelection();
    

});
        </script>

        <br>

        <h2>Projects</h2>

        <table class="table">
            <thead>
                <th>Project</th>
                <th>Location</th>
                <th>Area</th>
                <th>#</th>
                <th>#</th>
                <th>#</th>
               
            </thead>
            <tbody class="sortable_list2">
                <?php
                $projects = $veritabani->getAllProjects();
                $indexForeach2 = 0;
                foreach ($projects as $project) {
                    
                        # code...
                    
                        echo "<tr sort_id='".$indexForeach2."' id='".$project["project_id"]."'style='background-color:#ffff'>";
                    echo "<td> <img src='../images/move-icon.svg' style='height:30px;'>" . $project["project_title"] . "</td>";
                    echo "<td>" . $project["project_location"] . "</td>";
                    echo "<td>" . $project["project_area"] . "</td>";
                    echo '<td>';

                    if($project["isHomePage"] == 1) { 
                    echo     '
                        <form method="post" action="/admin/index.php">
                        <button type="submit" class="btn btn-danger">Remove Home Page</button>
                        <input type="hidden" name="category" value="project_home">
                        <input type="hidden" name="project_id" value="' . $project["project_id"] . '">
                        <input type="hidden" name="status" value="' . $project["isHomePage"] . '">
                        </form>
                    ';
                    } else { 
                    echo '
                    <form method="post" action="/admin/index.php">
                    <button type="submit" class="btn btn-success">Add Home Page</button>
                    <input type="hidden" name="category" value="project_home">
                    <input type="hidden" name="project_id" value="' . $project["project_id"] . '">
                    <input type="hidden" name="status" value="' . $project["isHomePage"] . '">
                    </form>
                    </td>';
                    }
                    echo "<td>";
                    echo '
                    <a href="/admin/project_edit.php?id=' . $project["project_id"] . '" class="btn btn-primary">Edit</a>';
                    echo "</td>";

                    echo "<td>";
                    echo '
                    <form method="post" action="/admin/index.php">
                    <button type="submit" class="btn btn-danger">Remove</button>
                    <input type="hidden" name="category" value="project_remove">
                    <input type="hidden" name="project_id" value="' . $project["project_id"] . '">
                    </form>';
                    echo "</td>";
                    $indexForeach2++;
                    

                    echo "</tr>";
                }

                
                ?>
            </tbody>
        </table>
        <script>
             $(function() {
$( ".sortable_list2" ).sortable({
    connectWith: ".connectedSortable2",
    update: function (event, ui) {
        var sorted = $( ".sortable_list2" );
        
        var data = sorted.sortable('toArray');
        $.post("index.php", {data:data, category:"project_sort"}, function(result){
            console.log(result);
        });
    },
    receive: function(event, ui) {
       console.log(ui);
    }         
}).disableSelection();
    

});
        </script>
    </div>




</div>

    <?php include('layout/footer.php'); ?>