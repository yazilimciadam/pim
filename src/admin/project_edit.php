<?php
session_start();
ob_start();
include "../classes/MainClass.php";
include("../classes/uploadImage/class.upload.php");
$veritabani = new MainClass();
$veritabani->sessionControl();

include('layout/header.php');

if (isset($_POST['category']) && ($_POST['category'] == "project_edit")) {
    //var_dump($_POST["old_detail_images"]);
    $imageBanner;
    $imagesDetailed;
    $arr = [];
    $imagethum;
    if ($_FILES["detail_images"]["size"][0] > 0) {
        $files = array();
        foreach ($_FILES['detail_images'] as $k => $l) {
            foreach ($l as $i => $v) {
                if (!array_key_exists($i, $files))
                    $files[$i] = array();
                $files[$i][$k] = $v;
            }
        }

        foreach ($files as $file) {

            $handle3 = new \Verot\Upload\Upload($file, 'tr_TR');
            if ($handle3->uploaded) {
                $handle3->file_new_name_body   = time();
                $handle3->file_src_pathname    = $file["tmp_name"];
                $handle3->image_ratio = true;
                $handle3->image_resize         = true;
                $handle3->image_x              = 600;
                $handle3->image_ratio_y        = true;
                $handle3->process("../images/");
                if ($handle3->processed) {
                    $arr[] = array(
                        "image" => "images/" . $handle3->file_dst_name,
                    );
                } else {
                    echo 'error : ' . $handle3->error;
                }
            }
            unset($handle3);
        }
        $imagesDetailed = json_encode($arr);
    } else {

        $imagesDetailed = $_POST["old_detail_images"];
       
    }
    if ($_FILES["banner_image"]["size"] > 0) {


        $handle = new \Verot\Upload\Upload($_FILES["banner_image"], 'tr_TR');
        if ($handle->uploaded) {
            $handle->file_new_name_body   = time();

            $handle->file_src_pathname    = $_FILES["banner_image"]["tmp_name"];
            $handle->process("../images/");
            if ($handle->processed) {
                $imageBanner =  "images/" . $handle->file_dst_name;
            } else {
                echo 'error : ' . $handle->error;
            }
        }
        unset($handle);
    } else {
        $imageBanner = $_POST["old_banner_image"];
    }

    if ($_FILES["thumb"]["size"] > 0) {


     
        $handle4 = new \Verot\Upload\Upload($_FILES["thumb"], 'tr_TR');
        
            if ($handle4->uploaded) {
                $handle4->file_new_name_body   = "aba".time()*233;
                $handle4->image_resize         = true;
                
                $handle4->image_ratio_crop = "T";
                $handle4->image_x              = 250;
                $handle4->image_y              = 180;
                
                $handle4->process("../images/");
                
            if ($handle4->processed) {
                $imagethum =  "images/" . $handle4->file_dst_name;
                
                
            } else {
                echo 'error : ' . $handle4->error;
            }
        }
        unset($handle4);
    } else {
        $imagethum = $_POST["old_thumb"];
    }
    

    $vv =  $veritabani->updateProject($_POST["id"], $_POST["title"], $_POST["area"], $_POST["location"], $imagesDetailed, $imageBanner,$imagethum);
    if($vv){
        echo "<script>alert('Update Başarılı');</script>";
       echo "<script>window.location.href='index.php';</script>";
    }
}
$proje = $veritabani->getProjects($_GET["id"]);


?>

<br>
<br>
<div class="row">

    <div class="col-md-12">

        <h2>Projects</h2>
        <form enctype="multipart/form-data" method="post" action="project_edit.php">

            <div class="form-group">
                <label for="exampleInputEmail1">Title</label>
                <input type="text" name="title" class="form-control" value="<?php echo $proje["project_title"]; ?>" placeholder="Project Title">
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Location</label>
                <input type="text" name="location" class="form-control" value="<?php echo $proje["project_location"]; ?>" placeholder="Project Location">
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Area</label>
                <input type="text" name="area" class="form-control" value="<?php echo $proje["project_area"]; ?>" placeholder="Project Area">
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Thumbnail</label>
                <input type="file" class="form-control" name="thumb" placeholder="Project Home Banner">
                <small>Projeler sayfasında görünen küçük fotoğraf</small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Home Banner</label>
                <input type="file" class="form-control" name="banner_image" placeholder="Project Home Banner">

                <small>Anasayfada Görünür ayarlanırsa bu fotograf kullanılır.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Images</label>
                <input type="file" name="detail_images[]" multiple class="form-control" placeholder="Project Images">
                <small>Proje Detayındaki Fotograflar.</small>
            </div>
            <input type="hidden" name="category" value="project_edit">
            <input type="hidden" name="old_banner_image" value="<?php echo $proje["project_banner"]; ?>">
            <input type="hidden" name="old_detail_images" value='<?php echo $proje["project_images"]; ?>'>
            <input type="hidden" name="old_thumb" value='<?php echo (isset($proje["thumbnail"]))?$proje["thumbnail"]:""; ?>'>
            <input type="hidden" name="id" value="<?php echo $proje["project_id"]; ?>">
            <button type="submit" class="btn btn-primary">Update Project</button>

        </form>



    </div>




</div>

<?php include('layout/footer.php'); ?>