<?php
session_start();
ob_start();
include "../classes/MainClass.php";
include("../classes/uploadImage/class.upload.php");
$veritabani = new MainClass();

include('layout/header.php');




if (isset($_POST['category']) && ($_POST['category'] == "person_up")) {

    if ($_FILES["banner_image"]["size"] > 0) {

    $imageBanner;
    $handle = new \Verot\Upload\Upload($_FILES["banner_image"], 'tr_TR');
    if ($handle->uploaded) {
        $handle->file_new_name_body   = time();
        $handle->image_resize         = true;
            
        $handle->image_ratio_crop = "T";
        $handle->image_x              = 275;
        $handle->image_y              = 300;
        $handle->file_src_pathname    = $_FILES["banner_image"]["tmp_name"];
        $handle->process("../images/");
        if ($handle->processed) {
            $imageBanner =  "images/" . $handle->file_dst_name;
        } else {
            echo 'error : ' . $handle->error;
        }
    }
        # code...
    }else{
        $imageBanner = $_POST["old_image"];

    }
    
    $vv =  $veritabani->updatePersona($_POST["person_id"], $_POST["name"], $_POST["text_about"],$imageBanner);
   
    if ($vv) {
        echo "<script>alert('Upload Success');</script>";
        echo "<script>window.location.href='personals.php';</script>";
    }
}
$person = $veritabani->getPersonaID($_GET["person_id"]);

?>

<br>
<br>

<script src="/js/tinymce/js/tinymce/tinymce.min.js"></script>

<script>tinymce.init({selector:'textarea'});</script>


<div class="row">


    <div class="col-md-12">

        <form enctype="multipart/form-data" method="post" action="add_persona.php">


        <div class="form-group">
                <label for="exampleInputEmail1">Name </label>
                <input type="text" class="form-control" name="name" value="<?php echo $person["name"] ?>">

            </div>

            <div class="form-group">
            <textarea name="text_about" id="textEditor" cols="30" rows="10">
            <?php  echo $person["text"]?>
            </textarea>
              

            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Avatar</label>
                <input type="file" class="form-control" name="banner_image" placeholder="Image">


            </div>
            <input type="hidden" name="old_image" value="<?php echo $person["image"] ?>">
            <br>

            <input type="hidden" name="category" value="person_up">
            <input type="hidden" name="person_id" value="<?php echo $person["per_id"] ?>" >
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
   


</div>

<?php include('layout/footer.php'); ?>