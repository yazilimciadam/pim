<?php
session_start();
ob_start();
include "../classes/MainClass.php";
include("../classes/uploadImage/class.upload.php");
$veritabani = new MainClass();
$veritabani->sessionControl();

include('layout/header.php');

if (isset($_POST['category']) && ($_POST['category'] == "persona_remove")) {

    $veritabani->deletePersona($_POST["person_id"]);
}


?>
<br>
<br>
<div class="row">

   <div class="col-md-3">
   <a href="/admin/add_personel.php" class="btn btn-success">Person Add</a>
   </div>

    <div class="col-md-12">

   
   

        <table class="table">
            <thead>
                <th>Name</th>
                <th>Image</th>
                <th>Text</th>
               
                <th>Edit</th>
                <th>Delete</th>

              
            </thead>

            <tbody>
                <?php
                $references = $veritabani->getPersona();
                foreach ($references as $reference) {
                    echo "<tr>";
                    echo '<td>' . $reference["name"] . '</td>';
                    echo '<td><img src="/' . $reference["image"] . '" width="100px"></td>';
                    echo '<td>' . $reference["text"] . '</td>';
                
            echo '<td>';
            echo '
            <form method="post" action="/admin/add_persona.php?person_id='.$reference["per_id"].'" >
            <button type="submit" class="btn btn-warning">Update</button>
            <input type="hidden" name="category" value="persona_remove">
            <input type="hidden" name="person_id" value="' . $reference["per_id"] . '">

            </form>
            </td>';
            echo '<td>';
            echo '
            <form method="post" action="/admin/personals.php" >
            <button type="submit" class="btn btn-danger">Delete</button>
            <input type="hidden" name="category" value="persona_remove">
            <input type="hidden" name="person_id" value="' . $reference["per_id"] . '">

            </form>
            </td>';

                    echo "</tr>";
                }
                ?>
            </tbody>

        </table>

    </div>



</div>

<?php include('layout/footer.php'); ?>