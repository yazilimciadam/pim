<nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
      <div class="position-sticky pt-3">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="index.php">
              <span data-feather="home"></span>
              Projects
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="reference.php">
              <span data-feather="file"></span>
              References
            </a>
          <li class="nav-item">
            <a class="nav-link" href="personals.php">
              <span data-feather="file"></span>
              Persons
            </a>
          </li>

          <li class="nav-item">
            <a class="nav-link" href="update_about.php">
              <span data-feather="file"></span>
              About and Site
            </a>
          </li>
          
          <li class="nav-item">
            <a class="nav-link" href="logout.php">
              <span data-feather="shopping-cart"></span>
              Logout          </a>
          </li>
          <hr>
          


         
        </ul>
      


       
      </div>
    </nav>