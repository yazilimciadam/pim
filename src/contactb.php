<?php if (!strstr(ini_get('disable_functions'), 'ini_set')) {
    ini_set('default_charset', 'UTF-8');
}
header('Content-Type: text/html; charset=UTF-8');
header('Cache-Control: no-store, no-cache, must-revalidate, max-age=0');
header('Cache-Control: post-check=0, pre-check=0', false);
header('Pragma: no-cache'); ?>
<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

<head>
    <meta charset="UTF-8">
    <title>CONTACT | PIM ARCHITECTS</title>
    <meta name="referrer" content="same-origin">
    <link rel="canonical" href="https://pimarchitects.com/contact.php">
    <meta name="google-site-verification" content="VCnOnu8apZKI4ETt2bXQiNohvVKTtrwlWJpzs4SX1tk">
    <meta name="robots" content="max-image-preview:large">
    <meta name="twitter:card" content="summary">
    <meta property="og:title" content="CONTACT | PIM ARCHITECTS">
    <meta property="og:type" content="website">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link rel="apple-touch-icon" sizes="180x180" href="images/apple-touch-icon-93bcce.png">
    <meta name="msapplication-TileImage" content="images/mstile-144x144-c1fe60.png">
    <link rel="manifest" href="manifest.json" crossOrigin="use-credentials">
    <link rel="mask-icon" href="website-icon-c7f47d.svg" color="rgb(0,0,0)">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-4705414-41"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-4705414-41', {
            'anonymize_ip': true,
            'forceSSL': true
        });
    </script>
    <link rel="alternate" hreflang="en" href="https://pimarchitects.com/contact.php">
    <link rel="stylesheet" href="css/site.c7f47d.css" type="text/css">
</head>

<body id="b1">
    <script>
        var p = document.createElement("P");
        p.innerHTML = "&nbsp;", p.style.cssText = "position:fixed;visible:hidden;font-size:100px;zoom:1", document.body.appendChild(p);
        var rsz = function(e) {
            return function() {
                var r = Math.trunc(1e3 / parseFloat(window.getComputedStyle(e).getPropertyValue("font-size"))) / 10,
                    t = document.body;
                r != t.style.getPropertyValue("--f") && t.style.setProperty("--f", r)
            }
        }(p);
        if ("ResizeObserver" in window) {
            var ro = new ResizeObserver(rsz);
            ro.observe(p)
        } else if ("requestAnimationFrame" in window) {
            var raf = function() {
                rsz(), requestAnimationFrame(raf)
            };
            requestAnimationFrame(raf)
        } else setInterval(rsz, 100);
    </script>

    <div class="ps1 v1 s1">
        <div class="v2 ps2 s2 c1">
            <div class="v2 ps3 s2 c2">
                <div class="v2 ps3 s3 c3">
                    <a href="#" class="a1">
                        <picture class="i2">
                            <source srcset="images/pim-logo-82.png 1x, images/pim-logo-164.png 2x" media="(max-width:479px)">
                            <source srcset="images/pim-logo-123.png 1x, images/pim-logo-246.png 2x" media="(max-width:767px)">
                            <source srcset="images/pim-logo-68.png 1x, images/pim-logo-136.png 2x" media="(max-width:959px)">
                            <source srcset="images/pim-logo-85.png 1x, images/pim-logo-170.png 2x" media="(max-width:1199px)">
                            <source srcset="images/pim-logo-106.png 1x, images/pim-logo-212.png 2x" media="(min-width:1200px)"><img src="images/pim-logo-212.png" class="un1 i1">
                        </picture>
                    </a>
                </div>
                <div id="menu" class="v3 ps4 s4 c4">
                    <p class="p1 f1"><a href="#">about us</a><span class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="f1"><a href="#">projects</a></span><span class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="f1"><a href="#">loop</a></span><span class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="f1"><a href="#">references</a></span><span class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="f1"><a href="#">contact</a></span></p>
                </div>
            </div>
        </div>
    </div>
    <div class="c5">
        <div class="ps5 v1 s5">
            <div class="v2 ps6 s6 c2">
                <div class="v2 ps7 s7 c6 map1 un26">
                </div>
                <div class="v4 ps8 s8 c7">
                    <ul class="menu-dropdown v5 ps3 s9 m1" id="m1">
                        <li class="v2 ps3 s9 mit1">
                            <div class="menu-content mcv1">
                                <div class="v2 ps3 s10 c8">
                                    <div class="v2 ps9 s11 c9">
                                        <p class="p2 f2">Menu</p>
                                    </div>
                                </div>
                            </div>
                            <ul class="menu-dropdown-1 v6 ps10 s12 m1" id="m2">
                                <li class="v2 ps3 s13 mit1">
                                    <a href="#" class="ml1">
                                        <div class="menu-content mcv2">
                                            <div class="v2 ps3 s14 c10">
                                                <div class="v2 ps11 s15 c11">
                                                    <p class="p2 f3">Home</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="v2 ps12 s13 mit1">
                                    <a href="#" class="ml1">
                                        <div class="menu-content mcv2">
                                            <div class="v2 ps3 s14 c12">
                                                <div class="v2 ps11 s15 c11">
                                                    <p class="p2 f3">About Us</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="v5 ps13 s16 mit1">
                                    <a href="#" class="ml1">
                                        <div class="menu-content mcv2">
                                            <div class="v2 ps3 s17 c13">
                                                <div class="v2 ps14 s18 c14">
                                                    <p class="p2 f4">Projects</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="v5 ps13 s19 mit1">
                                    <a href="#" class="ml1">
                                        <div class="menu-content mcv2">
                                            <div class="v2 ps3 s20 c15">
                                                <div class="v2 ps14 s21 c14">
                                                    <p class="p2 f4">References</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="v5 ps13 s22 mit1">
                                    <a href="#" class="ml1">
                                        <div class="menu-content mcv2">
                                            <div class="v2 ps3 s23 c16">
                                                <div class="v2 ps14 s24 c14">
                                                    <p class="p2 f4">Contact</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="v2 ps15 s25 c17">
                    <div class="wrapper1">
                        <style>
                            #menu a {}

                            #menu a:hover {
                                padding-bottom: 5px;
                                border-bottom: 5px solid #000000;
                            }

                            #references {
                                -webkit-filter: grayscale(100%);
                                filter: grayscale(100%);
                                transition: all 0.5s ease;
                            }

                            #references:hover {
                                -webkit-filter: grayscale(0%);
                                filter: grayscale(0%);
                                transition: all 0.2s ease;
                                cursor: pointer;
                                transform: scale(1.2) rotate(-2deg);
                                box-shadow: 0px 0px 50px #ccc;
                            }

                            #vitrin {
                                opacity: 1;
                                transition: all 0.2s ease;
                            }

                            #vitrin:hover {
                                opacity: 0;
                                cursor: pointer;
                                transition: all 0.5s ease;
                                transform: scale(0.9);
                                cursor: pointer;

                            }

                            #port {
                                transition: all 0.5s ease;
                                transform: scale(1);
                                box-shadow: 0px 0px 0px #ccc;
                            }

                            #port:hover {
                                cursor: pointer;
                                transition: all 0.5s ease;
                                transform: scale(0.9);
                                box-shadow: 0px 0px 50px #999;
                            }

                            #social {
                                opacity: 0.95;
                                transition: all 0.5s ease;
                            }

                            #social:hover {
                                opacity: 1;
                                transition: all 0.5s ease;
                            }
                        </style>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ps16 v1 s26">
        <div class="v2 ps17 s27 c2" style="
    display: flex;
    flex-direction: row;
">
            <div class="v2 ps3 s28 c18">
                <p class="p2 f5">ISTANBUL</p>
                <p class="p2 f6">Co-11 Ye&scedil;ilce Mh. Dalg&imath;&ccedil; Sok. No.11 Kat 1 Ka&gbreve;&imath;thane &Idot;stanbul / TURKEY</p>
                <p class="p2 f6"><a href="#"><br></a></p>
                <p class="p2 f6"><a href="#">+</a><span class="f6"><a href="tel:+902129444528">90 212 944 45 28</a></span></p>
                <p class="p2 f6"><a href="javascript:em1();">info@pimarchitects.com</a></p>
            </div>
            
            
            <div class="v2 ps18 s29 c19">
                <p class="p2 f5">TASHKENT</p>
                <p class="p2 f6">21a T.Shevchenko Street 10006&nbsp;</p>
                <p class="p2 f6">Tashkent / UZBEKISTAN</p>
                <p class="p2 f6"><a href="#"><br></a></p>
                <p class="p2 f6"><a href="tel:+998970051071">+998 97 005 10 71</a></p>
                <p class="p2 f6"><a href="javascript:em2();">tashkent@pimarchitects.com</a></p>
            </div>
        </div>
        <div class="v2 ps19 s30 c20">
            <div class="v2 ps3 s30 c2">
                <div class="v2 ps20 s31 c21">
                    <a href="https://www.facebook.com/PimArchitects/" target="_blank" rel="noopener" class="a1">
                        <picture id="social" class="i2">
                            <source srcset="images/facebook-2-31.png 1x, images/facebook-2-62.png 2x" media="(max-width:479px)">
                            <source srcset="images/facebook-2-47.png 1x, images/facebook-2-94.png 2x" media="(max-width:767px)">
                            <source srcset="images/facebook-2-15.png 1x, images/facebook-2-30.png 2x" media="(max-width:959px)">
                            <source srcset="images/facebook-2-19.png 1x, images/facebook-2-38.png 2x" media="(max-width:1199px)">
                            <source srcset="images/facebook-2-24.png 1x, images/facebook-2-48.png 2x" media="(min-width:1200px)"><img src="images/facebook-2-48.png" class="un2 i3">
                        </picture>
                    </a>
                </div>
                <div class="v2 ps21 s31 c22">
                    <a href="https://www.linkedin.com/company/pimarchitects/" target="_blank" rel="noopener" class="a1">
                        <picture id="social" class="i2">
                            <source srcset="images/linkedin-31.png 1x, images/linkedin-62.png 2x" media="(max-width:479px)">
                            <source srcset="images/linkedin-47.png 1x, images/linkedin-94.png 2x" media="(max-width:767px)">
                            <source srcset="images/linkedin-15.png 1x, images/linkedin-30.png 2x" media="(max-width:959px)">
                            <source srcset="images/linkedin-19.png 1x, images/linkedin-38.png 2x" media="(max-width:1199px)">
                            <source srcset="images/linkedin-24.png 1x, images/linkedin-48.png 2x" media="(min-width:1200px)"><img src="images/linkedin-48.png" class="un3 i3">
                        </picture>
                    </a>
                </div>
                <div class="v2 ps22 s31 c23">
                    <a href="https://instagram.com/pimarchitects" target="_blank" rel="noopener" class="a1">
                        <picture id="social" class="i2">
                            <source srcset="images/instagram-31.png 1x, images/instagram-62.png 2x" media="(max-width:479px)">
                            <source srcset="images/instagram-47.png 1x, images/instagram-94.png 2x" media="(max-width:767px)">
                            <source srcset="images/instagram-15.png 1x, images/instagram-30.png 2x" media="(max-width:959px)">
                            <source srcset="images/instagram-19.png 1x, images/instagram-38.png 2x" media="(max-width:1199px)">
                            <source srcset="images/instagram-24.png 1x, images/instagram-48.png 2x" media="(min-width:1200px)"><img src="images/instagram-48.png" class="un4 i3">
                        </picture>
                    </a>
                </div>
                <div class="v2 ps23 s32 c24">
                    <a href="javascript:em1();" class="a1">
                        <picture id="social" class="i2">
                            <source srcset="images/email-37.png 1x, images/email-74.png 2x" media="(max-width:479px)">
                            <source srcset="images/email-56.png 1x, images/email-112.png 2x" media="(max-width:767px)">
                            <source srcset="images/email-19.png 1x, images/email-38.png 2x" media="(max-width:959px)">
                            <source srcset="images/email-23.png 1x, images/email-46.png 2x" media="(max-width:1199px)">
                            <source srcset="images/email-29.png 1x, images/email-58.png 2x" media="(min-width:1200px)"><img src="images/email-58.png" class="un5 i4">
                        </picture>
                    </a>
                </div>
            </div>
        </div>
        <div class="v3 ps24 s33 c25"></div>
    </div>
    <div class="c26">
    </div>
    <script>
        dpth = "/"
    </script>
    <script type="text/javascript" src="js/jquery.b027e1.js"></script>
    <script type="text/javascript" src="js/jqueryui.b027e1.js"></script>
    <script type="text/javascript" src="js/menu.b027e1.js"></script>
    <script type="text/javascript" src="js/menu-dropdown-animations.b027e1.js"></script>
    <script type="text/javascript" src="js/menu-dropdown.c7f47d.js"></script>
    <script type="text/javascript" src="js/menu-dropdown-1.c7f47d.js"></script>
    <script type="text/javascript" src="js/contact.c7f47d.js"></script>
</body>

</html>