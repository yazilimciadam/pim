

<?php 

include "classes/MainClass.php";
$veritabani = new MainClass();
$site_info = $veritabani->getAbout(1);
$site_persona = $veritabani->getPersona();



?>

<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

<head>
    <meta charset="UTF-8">
    <title>ABOUT US | PIM ARCHITECTS</title>
    <meta name="referrer" content="same-origin">
    <link rel="canonical" href="https://pimarchitects.com/about-us.html">
    <meta name="google-site-verification" content="VCnOnu8apZKI4ETt2bXQiNohvVKTtrwlWJpzs4SX1tk">
    <meta name="robots" content="max-image-preview:large">
    <meta name="twitter:card" content="summary">
    <meta property="og:title" content="ABOUT US | PIM ARCHITECTS">
    <meta property="og:type" content="website">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <script>var currDev = function () { var c = window.getComputedStyle(document.getElementsByClassName('device')[0]).getPropertyValue('background-color'); return (c != undefined ? parseInt(c.substring(4)) : 0) }</script>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link rel="apple-touch-icon" sizes="180x180" href="images/apple-touch-icon-93bcce.png">
    <meta name="msapplication-TileImage" content="images/mstile-144x144-c1fe60.png">
    <link rel="manifest" href="manifest.json" crossOrigin="use-credentials">
    <link rel="mask-icon" href="website-icon-b04c2c.svg" color="rgb(0,0,0)">
    <script>window.gaf = function () { var e, t, a, n; e = "script", t = document, a = t.createElement(e), n = t.getElementsByTagName(e)[0], a.async = 1, a.src = "https://www.googletagmanager.com/gtag/js?id=UA-4705414-41", n.parentNode.insertBefore(a, n) }; if (!!document.cookie.match(/cookieConsent=true/)) window.gaf();</script>
    <script>window.dataLayer = window.dataLayer || []; function gtag() { dataLayer.push(arguments); } gtag('js', new Date()); gtag('config', 'UA-4705414-41', { 'anonymize_ip': true, 'forceSSL': true });</script>
    <link rel="alternate" hreflang="en" href="https://pimarchitects.com/about-us.html">
    <link rel="stylesheet" href="css/site.b04c2c.css" type="text/css">
</head>

<body id="b1">
    <div class="ps1 v1 s89">
        <div class="v2 ps2 s2 c38">
            <div class="v2 ps3 s2 c2">
                <div class="v2 ps3 s3 c39">
                    <a href="/" class="a1">
                        <picture class="i2">
                            <source srcset="images/pim-logo-82-1.png 1x, images/pim-logo-164-1.png 2x"
                                media="(max-width:479px)">
                            <source srcset="images/pim-logo-123-1.png 1x, images/pim-logo-246-1.png 2x"
                                media="(max-width:767px)">
                            <source srcset="images/pim-logo-68-1.png 1x, images/pim-logo-136-1.png 2x"
                                media="(max-width:959px)">
                            <source srcset="images/pim-logo-85-1.png 1x, images/pim-logo-170-1.png 2x"
                                media="(max-width:1199px)">
                            <source srcset="images/pim-logo-106-1.png 1x, images/pim-logo-212-1.png 2x"
                                media="(min-width:1200px)"><img src="images/pim-logo-212-1.png" class="un287 i1">
                        </picture>
                    </a>
                </div>
                <div id="menu" class="v3 ps4 s4 c40">
                    <p class="p1 f1"><a href="#">about us</a><span class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span
                            class="f1"><a href="projects.php">projects</a></span><span
                            class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="f1"><a
                                href="/">loop</a></span><span class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span
                            class="f1"><a href="references.html">references</a></span><span
                            class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="f1"><a
                                href="contact.php">contact</a></span></p>
                </div>
            </div>
        </div>
        <div class="v2 ps81 s90 c2">
            <div class="v2 ps82 s91 c110">
                <div class="p4 f15">
                    <?php echo $site_info["about"] ?></div>
              
               
            </div>
            <div class="v4 ps83 s17 c49">
                <ul class="menu-dropdown-1 v5 ps3 s18 m1" id="m11">
                    <li class="v2 ps3 s18 mit1">
                        <div class="menu-content mcv1">
                            <div class="v2 ps3 s19 c111">
                                <div class="v2 ps14 s20 c13">
                                    <p class="p2 f4">Menu</p>
                                </div>
                            </div>
                        </div>
                        <ul class="menu-dropdown v6 ps15 s21 m1" id="m12">
                            <li class="v2 ps3 s22 mit1">
                                <a href="/" class="ml1">
                                    <div class="menu-content mcv2">
                                        <div class="v2 ps3 s23 c112">
                                            <div class="v2 ps16 s24 c15">
                                                <p class="p2 f5">Home</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="v2 ps17 s22 mit1">
                                <a href="#" class="ml1">
                                    <div class="menu-content mcv2">
                                        <div class="v2 ps3 s23 c113">
                                            <div class="v2 ps16 s24 c15">
                                                <p class="p2 f5">About Us</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="v5 ps18 s22 mit1">
                                <a href="projects.php" class="ml1">
                                    <div class="menu-content mcv2">
                                        <div class="v2 ps3 s23 c114">
                                            <div class="v2 ps16 s24 c15">
                                                <p class="p2 f5">Projects</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="v5 ps18 s22 mit1">
                                <a href="references.html" class="ml1">
                                    <div class="menu-content mcv2">
                                        <div class="v2 ps3 s23 c115">
                                            <div class="v2 ps16 s24 c15">
                                                <p class="p2 f5">References</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="v5 ps18 s22 mit1">
                                <a href="contact.php" class="ml1">
                                    <div class="menu-content mcv2">
                                        <div class="v2 ps3 s23 c116">
                                            <div class="v2 ps16 s24 c15">
                                                <p class="p2 f5">Contact</p>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="v2 ps84 s28 c9">
                <div class="wrapper7">
                    <style>
                        #menu a {}

                        #menu a:hover {
                            padding-bottom: 5px;
                            border-bottom: 5px solid #000000;
                        }

                        #references {
                            -webkit-filter: grayscale(100%);
                            filter: grayscale(100%);
                            transition: all 0.5s ease;
                        }

                        #references:hover {
                            -webkit-filter: grayscale(0%);
                            filter: grayscale(0%);
                            transition: all 0.2s ease;
                            cursor: pointer;
                            transform: scale(1.2) rotate(-2deg);
                            box-shadow: 0px 0px 50px #ccc;
                        }

                        #vitrin {
                            opacity: 1;
                            transition: all 0.2s ease;
                        }

                        #vitrin:hover {
                            opacity: 0;
                            cursor: pointer;
                            transition: all 0.5s ease;
                            transform: scale(0.9);
                            cursor: pointer;

                        }

                        #port {
                            transition: all 0.5s ease;
                            transform: scale(1);
                            box-shadow: 0px 0px 0px #ccc;
                        }

                        #port:hover {
                            cursor: pointer;
                            transition: all 0.5s ease;
                            transform: scale(0.9);
                            box-shadow: 0px 0px 50px #999;
                        }

                        #social {
                            opacity: 0.95;
                            transition: all 0.5s ease;
                        }

                        #social:hover {
                            opacity: 1;
                            transition: all 0.5s ease;
                        }
                    </style>
                </div>
            </div>
        </div>
        <div class="v2 ps85 s92 c2" style="width:auto !important; display:flex;">

        <?php for ($i=0; $i < count($site_persona); $i++) { 
            $element = $site_persona[$i];
         
            ?>

            <div class="v2 ps3 s93 c35" style="margin-right:100px;">
                <picture class="i2">
                   
                    <img src="<?php echo $element["image"] ?>" class="un288 i19">
                </picture>
                <p class="p5 f16"><?php echo $element["name"] ?></p>
                <div class="p5 f17"><?php echo $element["text"] ?></div>
            </div>
            <?php } ?>
            
        </div>
        <div class="v2 ps87 s95 c2" style="width:auto !important;">
       
        </div>
        <div class="v2 ps90 s57 c119">
            <div class="v2 ps3 s57 c2">
                <div class="v2 ps3 s58 c120">
                    <p class="p3 f6">Co-11 Ye&scedil;ilce Mh. Dalg&imath;&ccedil; Sok. No.11 Kat 1
                        Ka&gbreve;&imath;thane &Idot;stanbul</p>
                </div>
                <div class="v2 ps50 s59 c121">
                    <p class="p3 f6"><a href="tel:+902129444528">+90 212 944 45 28</a></p>
                </div>
                <div class="v2 ps51 s32 c122">
                    <div class="v2 ps3 s32 c2">
                        <div class="v2 ps28 s63 c25">
                            <a href="<?php echo $site_info["facebook"] ?>" target="_blank" rel="noopener" class="a1">
                                <picture id="social" class="i2 un322">
                                    <source data-srcset="images/facebook-2-31.png 1x, images/facebook-2-62.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:479px)">
                                    <source data-srcset="images/facebook-2-46.png 1x, images/facebook-2-92.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:767px)">
                                    <source data-srcset="images/facebook-2-15.png 1x, images/facebook-2-30.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:959px)">
                                    <source data-srcset="images/facebook-2-20.png 1x, images/facebook-2-40.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:1199px)">
                                    <source data-srcset="images/facebook-2-24.png 1x, images/facebook-2-48.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(min-width:1200px)"><img src="images/facebook-2-48.png"
                                        class="un290 i11">
                                </picture>
                            </a>
                        </div>
                        <div class="v2 ps29 s73 c20">
                            <a href="<?php echo $site_info["linkedin"] ?>" target="_blank" rel="noopener"
                                class="a1">
                                <picture id="social" class="i2 un323">
                                    <source data-srcset="images/linkedin-31-1.png 1x, images/linkedin-62-1.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:479px)">
                                    <source data-srcset="images/linkedin-46-1.png 1x, images/linkedin-92-1.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:767px)">
                                    <source data-srcset="images/linkedin-15.png 1x, images/linkedin-30.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:959px)">
                                    <source data-srcset="images/linkedin-20-1.png 1x, images/linkedin-40-1.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:1199px)">
                                    <source data-srcset="images/linkedin-24-1.png 1x, images/linkedin-48-1.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(min-width:1200px)"><img src="images/linkedin-48-1.png"
                                        class="un291 i12">
                                </picture>
                            </a>
                        </div>
                        <div class="v2 ps30 s61 c123">
                            <a href="<?php echo $site_info["instagram"] ?>" target="_blank" rel="noopener" class="a1">
                                <picture id="social" class="i2 un324">
                                    <source data-srcset="images/instagram-31-1.png 1x, images/instagram-62-1.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:479px)">
                                    <source data-srcset="images/instagram-46-1.png 1x, images/instagram-92-1.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:767px)">
                                    <source data-srcset="images/instagram-15.png 1x, images/instagram-30.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:959px)">
                                    <source data-srcset="images/instagram-20-1.png 1x, images/instagram-40-1.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:1199px)">
                                    <source data-srcset="images/instagram-24-1.png 1x, images/instagram-48-1.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(min-width:1200px)"><img src="images/instagram-48-1.png"
                                        class="un292 i11">
                                </picture>
                            </a>
                        </div>
                        <div class="v2 ps31 s36 c124">
                            <a href="javascript:em1();" class="a1">
                                <picture id="social" class="i2 un325">
                                    <source data-srcset="images/email-37-1.png 1x, images/email-74-1.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:479px)">
                                    <source data-srcset="images/email-55-1.png 1x, images/email-110-1.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:767px)">
                                    <source data-srcset="images/email-19.png 1x, images/email-38.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:959px)">
                                    <source data-srcset="images/email-23-1.png 1x, images/email-46-1.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:1199px)">
                                    <source data-srcset="images/email-29-1.png 1x, images/email-58-1.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(min-width:1200px)"><img src="images/email-58-1.png" class="un293 i8">
                                </picture>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="consentBanner" class="v6 ps32 s37 c125">
        <div class="ps33 v1 s38">
            <div class="v2 ps34 s39 c35">
                <a href="javascript:void(0)" class="allowConsent f7 btn6 v7 s40">OK</a>
            </div>
            <div class="v2 ps35 s41 c36">
                <p class="p3 f8">This website makes use of cookies. Please see our <span class="f9"><a class="noConsent"
                            href="privacy-policy.html">privacy policy</a></span> for details.</p>
            </div>
        </div>
    </div>
    <div class="c37">
    </div>
    <div class="device"></div>
    <script>var p = document.createElement("P"); p.innerHTML = "&nbsp;", p.style.cssText = "position:fixed;visible:hidden;font-size:100px;zoom:1", document.body.appendChild(p); var rsz = function (e) { return function () { var r = Math.trunc(1e3 / parseFloat(window.getComputedStyle(e).getPropertyValue("font-size"))) / 10, t = document.body; r != t.style.getPropertyValue("--f") && t.style.setProperty("--f", r) } }(p); if ("ResizeObserver" in window) { var ro = new ResizeObserver(rsz); ro.observe(p) } else if ("requestAnimationFrame" in window) { var raf = function () { rsz(), requestAnimationFrame(raf) }; requestAnimationFrame(raf) } else setInterval(rsz, 100);</script>

    <script>dpth = "/"</script>
    <script type="text/javascript" src="js/jquery.a5d7fb.js"></script>
    <script type="text/javascript" src="js/jqueryui.a5d7fb.js"></script>
    <script type="text/javascript" src="js/menu.a5d7fb.js"></script>
    <script type="text/javascript" src="js/menu-dropdown-animations.a5d7fb.js"></script>
    <script type="text/javascript" src="js/menu-dropdown-1.b04c2c.js"></script>
    <script type="text/javascript" src="js/menu-dropdown.b04c2c.js"></script>
    <script type="text/javascript" src="js/consent.a5d7fb.js"></script>
    <script type="text/javascript" src="js/about-us.b04c2c.js"></script>
</body>

</html>