

<?php 

include "classes/MainClass.php";
$veritabani = new MainClass();
$site_info = $veritabani->getAbout(1);
$site_persona = $veritabani->getPersona();



?>


<!DOCTYPE html>
<html lang="en" prefix="og: http://ogp.me/ns#">

<head>
    <meta charset="UTF-8">
    <title>ABOUT US | PIM ARCHITECTS</title>
    <meta name="referrer" content="same-origin">
    <link rel="canonical" href="https://pimarchitects.com/about-us.html">
    <meta name="google-site-verification" content="VCnOnu8apZKI4ETt2bXQiNohvVKTtrwlWJpzs4SX1tk">
    <meta name="robots" content="max-image-preview:large">
    <meta name="twitter:card" content="summary">
    <meta property="og:title" content="ABOUT US | PIM ARCHITECTS">
    <meta property="og:type" content="website">
    <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
    <link rel="apple-touch-icon" sizes="180x180" href="images/apple-touch-icon-93bcce.png">
    <meta name="msapplication-TileImage" content="images/mstile-144x144-c1fe60.png">
    <link rel="manifest" href="manifest.json" crossOrigin="use-credentials">
    <link rel="mask-icon" href="website-icon-4904a3.svg" color="rgb(0,0,0)">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-4705414-41"></script>
    <script>window.dataLayer = window.dataLayer || []; function gtag() { dataLayer.push(arguments); } gtag('js', new Date()); gtag('config', 'UA-4705414-41', { 'anonymize_ip': true, 'forceSSL': true });</script>
    <link rel="alternate" hreflang="en" href="https://pimarchitects.com/about-us.html">
    <link rel="stylesheet" href="css/site.4904a3.css" type="text/css">
</head>

<body id="b1">
    <script>var p = document.createElement("P"); p.innerHTML = "&nbsp;", p.style.cssText = "position:fixed;visible:hidden;font-size:100px;zoom:1", document.body.appendChild(p); var rsz = function (e) { return function () { var r = Math.trunc(1e3 / parseFloat(window.getComputedStyle(e).getPropertyValue("font-size"))) / 10, t = document.body; r != t.style.getPropertyValue("--f") && t.style.setProperty("--f", r) } }(p); if ("ResizeObserver" in window) { var ro = new ResizeObserver(rsz); ro.observe(p) } else if ("requestAnimationFrame" in window) { var raf = function () { rsz(), requestAnimationFrame(raf) }; requestAnimationFrame(raf) } else setInterval(rsz, 100);</script>

    <div class="ps1 v1 s1">
        <div class="v2 ps2 s2 c1">
            <div class="v2 ps3 s2 c2">
                <div class="v2 ps3 s3 c3">
                    <a href="/" class="a1">
                        <picture class="i2">
                            <source srcset="images/pim-logo-82.png 1x, images/pim-logo-164.png 2x"
                                media="(max-width:479px)">
                            <source srcset="images/pim-logo-123.png 1x, images/pim-logo-246.png 2x"
                                media="(max-width:767px)">
                            <source srcset="images/pim-logo-68.png 1x, images/pim-logo-136.png 2x"
                                media="(max-width:959px)">
                            <source srcset="images/pim-logo-85.png 1x, images/pim-logo-170.png 2x"
                                media="(max-width:1199px)">
                            <source srcset="images/pim-logo-106.png 1x, images/pim-logo-212.png 2x"
                                media="(min-width:1200px)"><img src="images/pim-logo-212.png" class="un1 i1">
                        </picture>
                    </a>
                </div>
                <div id="menu" class="v3 ps4 s4 c4">
                    <p class="p1 f1"><a href="about-us.php">about us</a><span class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span
                            class="f1"><a href="projects.php">projects</a></span><span class="f1">&nbsp;&nbsp;&nbsp;&nbsp;
                        </span><span class="f1"><a href="/">loop</a></span><span class="f1">&nbsp;&nbsp;&nbsp;&nbsp;
                        </span><span class="f1"><a href="references.php">references</a></span><span
                            class="f1">&nbsp;&nbsp;&nbsp;&nbsp; </span><span class="f1"><a
                                href="contact.php">contact</a></span></p>
                </div>
            </div>
        </div>
    </div>
    <div class="c5">
        <div class="ps5 v1 s5">
            <div class="v2 ps6 s6 c2">
                <div class="v2 ps7 s7 c6">
                    <div class="p2 f2"><?php echo $site_info["about"] ?></div>
                </div>
                <div class="v4 ps8 s8 c7">
                    <ul class="menu-dropdown-1 v5 ps3 s9 m1" id="m2">
                        <li class="v2 ps3 s9 mit1">
                            <div class="menu-content mcv1">
                                <div class="v2 ps3 s10 c8">
                                    <div class="v2 ps9 s11 c9">
                                        <p class="p3 f3">Menu</p>
                                    </div>
                                </div>
                            </div>
                            <ul class="menu-dropdown v6 ps10 s12 m1" id="m1">
                                <li class="v2 ps3 s13 mit1">
                                    <a href="/" class="ml1">
                                        <div class="menu-content mcv2">
                                            <div class="v2 ps3 s14 c10">
                                                <div class="v2 ps11 s15 c11">
                                                    <p class="p3 f4">Home</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="v2 ps12 s13 mit1">
                                    <a href="about-us.php" class="ml1">
                                        <div class="menu-content mcv2">
                                            <div class="v2 ps3 s14 c12">
                                                <div class="v2 ps11 s15 c11">
                                                    <p class="p3 f4">About Us</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="v5 ps13 s16 mit1">
                                    <a href="projects.php" class="ml1">
                                        <div class="menu-content mcv2">
                                            <div class="v2 ps3 s17 c13">
                                                <div class="v2 ps14 s18 c14">
                                                    <p class="p3 f5">Projects</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="v5 ps13 s19 mit1">
                                    <a href="references.php" class="ml1">
                                        <div class="menu-content mcv2">
                                            <div class="v2 ps3 s20 c15">
                                                <div class="v2 ps14 s21 c14">
                                                    <p class="p3 f5">References</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li class="v5 ps13 s22 mit1">
                                    <a href="contact.php" class="ml1">
                                        <div class="menu-content mcv2">
                                            <div class="v2 ps3 s23 c16">
                                                <div class="v2 ps14 s24 c14">
                                                    <p class="p3 f5">Contact</p>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="v2 ps15 s25 c17">
                    <div class="wrapper1">
                        <style>
                            #menu a {}

                            #menu a:hover {
                                padding-bottom: 5px;
                                border-bottom: 5px solid #000000;
                            }

                            #references {
                                -webkit-filter: grayscale(100%);
                                filter: grayscale(100%);
                                transition: all 0.5s ease;
                            }

                            #references:hover {
                                -webkit-filter: grayscale(0%);
                                filter: grayscale(0%);
                                transition: all 0.2s ease;
                                cursor: pointer;
                                transform: scale(1.2) rotate(-2deg);
                                box-shadow: 0px 0px 50px #ccc;
                            }

                            #vitrin {
                                opacity: 1;
                                transition: all 0.2s ease;
                            }

                            #vitrin:hover {
                                opacity: 0;
                                cursor: pointer;
                                transition: all 0.5s ease;
                                transform: scale(0.9);
                                cursor: pointer;

                            }

                            #port {
                                transition: all 0.5s ease;
                                transform: scale(1);
                                box-shadow: 0px 0px 0px #ccc;
                            }

                            #port:hover {
                                cursor: pointer;
                                transition: all 0.5s ease;
                                transform: scale(0.9);
                                box-shadow: 0px 0px 50px #999;
                            }

                            #social {
                                opacity: 0.95;
                                transition: all 0.5s ease;
                            }

                            #social:hover {
                                opacity: 1;
                                transition: all 0.5s ease;
                            }
                        </style>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="ps16 v1 s26" style="min-height: auto;">
        <div class="v2 ps17 s27 c2" style="
        display: flex;
        flex-direction: row;
        flex-wrap: wrap;
    ">
    <?php for ($i=0; $i < count($site_persona); $i++) { 
            $element = $site_persona[$i];
            if($i % 2 == 0){ ?>
 <div class="v2 ps3 s28 c2" style="
    margin-bottom: 40px;
">
                <div class="v2 ps3 s29 c18">
                    <picture class="i2">
                       
                        <img src="<?php echo $element["image"] ?>" class="un2 i3">
                    </picture>
                </div>
                <div class="v2 ps18 s30 c19" style="height: auto;">
                    <p class="p4 f6"><?php echo $element["name"] ?></p>
                    <div class="p4 f7"><?php echo $element["text"] ?></div>
                </div>
            </div>
      
         <?php    }else{ ?>
            
       <div style="margin-left: 30px;" class="v2 ps3 s28 c2">
                <div class="v2 ps3 s29 c18">
                    <picture class="i2">
                       
                        <img src="<?php echo $element["image"] ?>" class="un2 i3">
                    </picture>
                </div>
                <div class="v2 ps18 s30 c19" style="overflow: auto;">
                    <p class="p4 f6"><?php echo $element["name"] ?></p>
                    <div class="p4 f7"><?php echo $element["text"] ?></div>
                </div>
            </div>
      
         

            <?php } }?>
            
        </div>
        
        <div class="v2 ps23 s39 c22">
            <div class="v2 ps3 s39 c2">
                <div class="v2 ps3 s40 c23">
                    <p class="p5 f8">Co-11 Ye&scedil;ilce Mh. Dalg&imath;&ccedil; Sok. No.11 Kat 1
                        Ka&gbreve;&imath;thane &Idot;stanbul</p>
                </div>
                <div class="v2 ps24 s41 c24">
                    <p class="p5 f8"><a href="tel:+902129444528">+90 212 944 45 28</a></p>
                </div>
                <div class="v2 ps25 s42 c25">
                    <div class="v2 ps3 s42 c2">
                        <div class="v2 ps26 s43 c26" >
                            <a href="https://www.facebook.com/PimArchitects/" target="_blank" rel="noopener" class="a1">
                                <picture id="social" class="i2 un48">
                                    <source data-srcset="images/facebook-2-31.png 1x, images/facebook-2-62.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:479px)">
                                    <source data-srcset="images/facebook-2-47.png 1x, images/facebook-2-94.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:767px)">
                                    <source data-srcset="images/facebook-2-15.png 1x, images/facebook-2-30.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:959px)">
                                    <source data-srcset="images/facebook-2-19.png 1x, images/facebook-2-38.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:1199px)">
                                    <source data-srcset="images/facebook-2-24.png 1x, images/facebook-2-48.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(min-width:1200px)"><img src="images/facebook-2-48.png" class="un6 i7">
                                </picture>
                            </a>
                        </div>
                        <div class="v2 ps27 s43 c27">
                            <a href="https://www.linkedin.com/company/pimarchitects/" target="_blank" rel="noopener"
                                class="a1">
                                <picture id="social" class="i2 un49">
                                    <source data-srcset="images/linkedin-31.png 1x, images/linkedin-62.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:479px)">
                                    <source data-srcset="images/linkedin-47.png 1x, images/linkedin-94.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:767px)">
                                    <source data-srcset="images/linkedin-15.png 1x, images/linkedin-30.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:959px)">
                                    <source data-srcset="images/linkedin-19.png 1x, images/linkedin-38.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:1199px)">
                                    <source data-srcset="images/linkedin-24.png 1x, images/linkedin-48.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(min-width:1200px)"><img src="images/linkedin-48.png" class="un7 i7">
                                </picture>
                            </a>
                        </div>
                        <div class="v2 ps28 s43 c28">
                            <a href="https://instagram.com/pimarchitects" target="_blank" rel="noopener" class="a1">
                                <picture id="social" class="i2 un50">
                                    <source data-srcset="images/instagram-31.png 1x, images/instagram-62.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:479px)">
                                    <source data-srcset="images/instagram-47.png 1x, images/instagram-94.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:767px)">
                                    <source data-srcset="images/instagram-15.png 1x, images/instagram-30.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:959px)">
                                    <source data-srcset="images/instagram-19.png 1x, images/instagram-38.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:1199px)">
                                    <source data-srcset="images/instagram-24.png 1x, images/instagram-48.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(min-width:1200px)"><img src="images/instagram-48.png" class="un8 i7">
                                </picture>
                            </a>
                        </div>
                        <div class="v2 ps29 s44 c29">
                            <a href="javascript:em1();" class="a1">
                                <picture id="social" class="i2 un51">
                                    <source data-srcset="images/email-37.png 1x, images/email-74.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:479px)">
                                    <source data-srcset="images/email-56.png 1x, images/email-112.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:767px)">
                                    <source data-srcset="images/email-19.png 1x, images/email-38.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:959px)">
                                    <source data-srcset="images/email-23.png 1x, images/email-46.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(max-width:1199px)">
                                    <source data-srcset="images/email-29.png 1x, images/email-58.png 2x"
                                        srcset="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7"
                                        media="(min-width:1200px)"><img src="images/email-58.png" class="un9 i8">
                                </picture>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="c30">
    </div>
    <script>dpth = "/"</script>
    <script type="text/javascript" src="js/jquery.07169e.js"></script>
    <script type="text/javascript" src="js/jqueryui.07169e.js"></script>
    <script type="text/javascript" src="js/menu.07169e.js"></script>
    <script type="text/javascript" src="js/menu-dropdown-animations.07169e.js"></script>
    <script type="text/javascript" src="js/menu-dropdown.4904a3.js"></script>
    <script type="text/javascript" src="js/menu-dropdown-1.4904a3.js"></script>
    <script type="text/javascript" src="js/about-us.4904a3.js"></script>
</body>

</html>